package name.adty.mobiledashboard.object;

/**
 * Created by KLOPOS on 11/28/2016.
 */

public class ReportObject {
    String name;
    int drawableID;

    public ReportObject(String name, int drawableID) {
        this.name = name;
        this.drawableID = drawableID;
    }

    public ReportObject() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawableID() {
        return drawableID;
    }

    public void setDrawableID(int drawableID) {
        this.drawableID = drawableID;
    }
}
