package name.adty.mobiledashboard.object;

/**
 * Created by KLOPOS on 11/28/2016.
 */

public class LoginObject {
    String Token;
    String IMEI;
    String UserID;
    String Password;

    public LoginObject() {
    }

    public LoginObject(String token, String IMEI, String userID, String password) {
        Token = token;
        this.IMEI = IMEI;
        UserID = userID;
        Password = password;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
