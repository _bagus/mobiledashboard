package name.adty.mobiledashboard.report.filter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FilterReportBudgetControl extends AppCompatActivity {

    Spinner spinnerProject;
    TextView buttonCalendar;
    final String TAG = "MDFilterReportBudgetControl";
    String AsOf;
    String project = "";
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    String IMEI = "";
    List<String> dataProject = new ArrayList<String>();
    List<String> dataProjectID = new ArrayList<String>();
    static final String namaBulan[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    static final String namaBulanFull[] = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mobileDashboard = new MobileDashboard(getApplicationContext());
        sp = getSharedPreferences("MobileDashboard", MODE_PRIVATE);
        AsOf = String.valueOf(Calendar.getInstance().get(Calendar.DATE)) + "-" + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + "-" + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        IMEI = mobileDashboard.getIMEI();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_report_budget_control);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final LayoutInflater layoutInflater = getLayoutInflater();

        final ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataProject);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProject = (Spinner) findViewById(R.id.spinnerProject);
        spinnerProject.setAdapter(adapterProject);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetProject" +
                        "?Token=" + mobileDashboard.MD5(IMEI + "poteam16") +
                        "&IMEI=" + IMEI +
                        "&UserID=" + sp.getString("MDUserID", ""))
            .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterReportBudgetControl.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataProject.add(responseArray.getJSONObject(i).getString("NamaProject"));
                        dataProjectID.add(responseArray.getJSONObject(i).getString("Project"));
                    }
                    project = responseArray.getJSONObject(0).getString("Project");
                    FilterReportBudgetControl.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterProject.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterReportBudgetControl.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        spinnerProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                project = dataProjectID.get((int) l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button buttonSearch = (Button) findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra("AsOf", AsOf);
                i.putExtra("project", project);
                setResult(RESULT_OK, i);
                finish();
            }
        });

        buttonCalendar = (TextView) findViewById(R.id.buttonCalendar);
        buttonCalendar.setText(String.valueOf(Calendar.getInstance().get(Calendar.DATE)) + " " + namaBulanFull[Calendar.getInstance().get(Calendar.MONTH)] + " " + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
        buttonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(FilterReportBudgetControl.this, R.style.customDatePickerDialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                buttonCalendar.setText(dayOfMonth + " "
                                        + namaBulanFull[monthOfYear] + " " + year);
                                AsOf = dayOfMonth + "-"
                                        + namaBulan[monthOfYear] + "-" + year;

                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        Fabric.with(this, new Crashlytics());

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Filter Report Budget Control"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_filter_general, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.resetFilter) {
            project = "";
            spinnerProject.setSelection(0);
            AsOf = String.valueOf(Calendar.getInstance().get(Calendar.DATE)) + "-" + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + "-" + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            buttonCalendar.setText(String.valueOf(Calendar.getInstance().get(Calendar.DATE)) + " " + namaBulanFull[Calendar.getInstance().get(Calendar.MONTH)] + " " + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
