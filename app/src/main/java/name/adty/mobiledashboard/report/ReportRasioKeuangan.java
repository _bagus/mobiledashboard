package name.adty.mobiledashboard.report;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import name.adty.mobiledashboard.report.filter.FilterReportGeneral;
import name.adty.mobiledashboard.report.filter.FilterReportRasioKeuangan;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ReportRasioKeuangan extends AppCompatActivity {

    private final static String TAG = "MDReportRasioKeuangan";
    ProgressBar progressBar;
    ListView listReportRasioKeuangan;
    MobileDashboard mobileDashboard;
    List<ReportRasioKeuanganItem> items = new ArrayList<ReportRasioKeuanganItem>();
    ReportRasioKeuanganAdapter adapterReportRasioKeuangan;
    String downloadedFileName = "";
    SharedPreferences sp;

    @Override
    protected void onResume() {
        super.onResume();

        progressBar.setVisibility(View.VISIBLE);

        String IMEI = mobileDashboard.getIMEI();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetListLapRasioKeuangan?IMEI=" + IMEI + "&Token="
                        + mobileDashboard.getToken() + "&UserID=" + sp.getString("MDUserID", ""))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                JSONArray itemArray;
                try {
                    itemArray = new JSONArray(responseBody);
                    for (int i = 0; i < itemArray.length(); i++) {
                        ReportRasioKeuanganItem item = new ReportRasioKeuanganItem();
                        item.setNama(itemArray.getJSONObject(i).getString("Laporan"));
                        item.setLink(itemArray.getJSONObject(i).getString("Link"));
                        items.add(item);
                    }

                    ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterReportRasioKeuangan.notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                } catch (JSONException e) {
                    ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_rasio_keuangan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobileDashboard = new MobileDashboard(getApplicationContext());
        sp = getSharedPreferences("MobileDashboard", MODE_PRIVATE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        listReportRasioKeuangan = (ListView) findViewById(R.id.listReportRasioKeuangan);
        adapterReportRasioKeuangan = new ReportRasioKeuanganAdapter(getApplicationContext(), items);
        listReportRasioKeuangan.setAdapter(adapterReportRasioKeuangan);

        listReportRasioKeuangan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final ReportRasioKeuanganItem item = items.get(i);
                final AlertDialog.Builder passwordDialog = new AlertDialog.Builder(ReportRasioKeuangan.this);
                final LayoutInflater layoutInflater = getLayoutInflater();
                passwordDialog.setView(layoutInflater.inflate(R.layout.password_dialog, null)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                final AlertDialog alertDialog = passwordDialog.create();
                alertDialog.show();
                Button buttonPositive = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                final EditText editTextPassword = (EditText) alertDialog.findViewById(R.id.editTextPassword);
                buttonPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new OkHttpClient().newCall(new Request.Builder()
                                .url(mobileDashboard.APIURL + "/CekPassword" +
                                        "?Token=" + mobileDashboard.getToken() + "&UserID=" + sp.getString("MDUserID", "")
                                        + "&IMEI=" + mobileDashboard.getIMEI() + "&Password=" + mobileDashboard.MD5(editTextPassword.getText().toString()))
                                .method("POST", RequestBody.create(null, new  byte[0]))
                                .build())
                                .enqueue(new Callback() {
                                    @Override
                                    public void onFailure(Call call, final IOException e) {
                                        ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Error Conneciton", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onResponse(Call call, final Response response) throws IOException {
                                        JSONObject responseObject = null;
                                        try {
                                            responseObject = new JSONObject(response.body().string());
                                            if (responseObject.get("Value").toString().equals("1")) {
                                                String[] splitFileName = item.getLink().split("/");
                                                DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(item.getLink()));
                                                downloadRequest.setDescription(item.getNama());
                                                downloadRequest.setTitle(splitFileName[splitFileName.length - 1]);
                                                downloadRequest.allowScanningByMediaScanner();
                                                downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                                downloadRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, splitFileName[splitFileName.length - 1]);
                                                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                                                downloadManager.enqueue(downloadRequest);
                                                downloadedFileName = splitFileName[splitFileName.length - 1];
                                                registerReceiver(broadcastReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                                                alertDialog.dismiss();
                                                ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getApplicationContext(), "Download has started", Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                            } else {
                                                final JSONObject finalResponseObject = responseObject;
                                                ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            Toast.makeText(getApplicationContext(), finalResponseObject.get("Value").toString(), Toast.LENGTH_LONG).show();
                                                        } catch (JSONException e) {
                                                            Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                });
                                            }
                                        } catch (JSONException e) {
                                            final JSONObject finalResponseObject1 = responseObject;
                                            ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        Toast.makeText(getApplicationContext(), finalResponseObject1.get("Value").toString(), Toast.LENGTH_LONG).show();
                                                    } catch (JSONException e) {
                                                        Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                    }
                });
            }
        });

        Fabric.with(this, new Crashlytics());

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Report Rasio Keuangan"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_neraca, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.filter) {
            Intent filter = new Intent(ReportRasioKeuangan.this, FilterReportRasioKeuangan.class);
            startActivityForResult(filter, 1);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                progressBar.setVisibility(View.VISIBLE);
                String IMEI = mobileDashboard.getIMEI();

                new OkHttpClient().newCall(
                        new Request.Builder()
                                .url(mobileDashboard.APIURL + "/GetLapRasioKeuangan?IMEI="
                                        + IMEI + "&Token=" + mobileDashboard.getToken()
                                        + "&Pers=" + data.getStringExtra("perusahaan")
                                        + "&Project=" + data.getStringExtra("project")
                                        + "&Bulan=" + data.getStringExtra("bulan")
                                        + "&Tahun=" + data.getStringExtra("tahun") +
                                        "&UserID=" + sp.getString("MDUserID", ""))
                                .build()
                ).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        items.clear();
                        final String responseBody = response.body().string();
                        JSONArray itemArray;
                        try {
                            itemArray = new JSONArray(responseBody);
                            for (int i = 0; i < itemArray.length(); i++) {
                                ReportRasioKeuanganItem item = new ReportRasioKeuanganItem();
                                item.setNama(itemArray.getJSONObject(i).getString("NamaLaporan"));
                                item.setLink(itemArray.getJSONObject(i).getString("Link"));
                                items.add(item);
                            }

                            ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapterReportRasioKeuangan.notifyDataSetChanged();
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        } catch (JSONException e) {
                            ReportRasioKeuangan.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String title = "";
                                    String message = "";

                                    try {
                                        message = new JSONObject(responseBody).getString("Value");
                                        title = "Warning";
                                    } catch (JSONException e1) {
                                        message = e1.getMessage();
                                        title = "API Response Error";
                                    }

                                    new AlertDialog.Builder(ReportRasioKeuangan.this)
                                            .setTitle(title)
                                            .setMessage(message)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            })
                                            .show();
                                }
                            });
                        }
                    }
                });
            }
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mobileDashboard.openDownloadedFile(downloadedFileName);
        }
    };
}
