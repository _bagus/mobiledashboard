package name.adty.mobiledashboard.report.filter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FilterReportPengadaan extends AppCompatActivity {

    final String TAG = "MDFilterReportPengadaan";
    String dariTanggal;
    String sampaiTanggal;
    String project = "";
    String HRDept = "";
    String tipe = "";
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    String IMEI = "";
    List<String> dataHRDept = new ArrayList<String>();
    List<String> dataHRDeptID = new ArrayList<String>();
    List<String> dataProject = new ArrayList<String>();
    List<String> dataProjectID = new ArrayList<String>();
    List<String> dataTipe = new ArrayList<String>();
    List<String> dataTipeID = new ArrayList<String>();

    TextView textDari;
    TextView textSampai;
    Spinner spinnerHRDept;
    Spinner spinnerTipe;
    Spinner spinnerProject;

    static final String namaBulan[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mobileDashboard = new MobileDashboard(getApplicationContext());
        sp = getSharedPreferences("MobileDashboard", MODE_PRIVATE);
        IMEI = mobileDashboard.getIMEI();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_report_pengadaan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final LayoutInflater layoutInflater = getLayoutInflater();

        LinearLayout buttonDatePicker2 = (LinearLayout) findViewById(R.id.buttonDatePicker2);
        LinearLayout buttonDatePicker = (LinearLayout) findViewById(R.id.buttonDatePicker);

        dariTanggal = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1)
                + "-" + String.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)) + "-"
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        sampaiTanggal = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1)
                + "-" + String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) + "-"
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        textDari = (TextView) findViewById(R.id.textDari);
        textDari.setText(String.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)) + " "
                + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
        textSampai = (TextView) findViewById(R.id.textSampai);
        textSampai.setText(String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) + " "
                + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));

        buttonDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dpd = new DatePickerDialog(FilterReportPengadaan.this, R.style.customDatePickerDialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                dariTanggal = String.valueOf(monthOfYear + 1)
                                        + "-" + String.valueOf(dayOfMonth) + "-"
                                        + String.valueOf(year);
                                textDari.setText(String.valueOf(dayOfMonth) + " "
                                        + namaBulan[monthOfYear] + " " + String.valueOf(year));
                            }
                        },
                        Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR)),
                        Integer.valueOf(Calendar.getInstance().get(Calendar.MONTH)),
                        Integer.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH))
                );

                dpd.show();
            }
        });

        buttonDatePicker2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(FilterReportPengadaan.this, R.style.customDatePickerDialog,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                sampaiTanggal = String.valueOf(monthOfYear + 1)
                                        + "-" + String.valueOf(dayOfMonth) + "-"
                                        + String.valueOf(year);
                                textSampai.setText(String.valueOf(dayOfMonth) + " "
                                        + namaBulan[monthOfYear] + " " + String.valueOf(year));
                            }
                        },
                        Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR)),
                        Integer.valueOf(Calendar.getInstance().get(Calendar.MONTH)),
                        Integer.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH))
                );

                dpd.show();
            }
        });

        final ArrayAdapter<String> adapterHRDept = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataHRDept);
        adapterHRDept.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHRDept = (Spinner) findViewById(R.id.spinnerHRDept);
        spinnerHRDept.setAdapter(adapterHRDept);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetHRDept" +
                        "?Token=" + mobileDashboard.MD5(IMEI + "poteam16") +
                        "&IMEI=" + IMEI +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataHRDept.add(responseArray.getJSONObject(i).getString("NamaDept"));
                        dataHRDeptID.add(responseArray.getJSONObject(i).getString("DivisiPemohon"));
                    }
                    HRDept = responseArray.getJSONObject(0).getString("NamaDept");
                    FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterHRDept.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } finally {
                    response.close();
                }
            }
        });

        final ArrayAdapter<String> adapterTipe = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataTipe);
        adapterTipe.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipe = (Spinner) findViewById(R.id.spinnerTipe);
        spinnerTipe.setAdapter(adapterTipe);
        new OkHttpClient().newCall(
                new Request.Builder().url(mobileDashboard.APIURL + "/GetTipePengadaan" +
                        "?Token=" + mobileDashboard.MD5(IMEI + "poteam16") +
                        "&IMEI=" + IMEI +
                        "&UserID=" + sp.getString("MDUserID", "")).build()
        ).enqueue(
                new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        final String responseBody = response.body().string();
                        try {
                            JSONArray responseArray = new JSONArray(responseBody);
                            for (int i = 0; i < responseArray.length(); i++) {
                                dataTipe.add(responseArray.getJSONObject(i).getString("Ket"));
                                dataTipeID.add(responseArray.getJSONObject(i).getString("Tipe"));
                            }
                            tipe = responseArray.getJSONObject(0).getString("Tipe");
                            FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapterTipe.notifyDataSetChanged();
                                }
                            });
                        } catch (JSONException e) {
                            FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                                    } catch (JSONException e1) {
                                        Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        } finally {
                            response.close();
                        }
                    }
                }
        );

        final ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataProject);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProject = (Spinner) findViewById(R.id.spinnerProject);
        spinnerProject.setAdapter(adapterProject);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetProject" +
                        "?Token=" + mobileDashboard.MD5(IMEI + "poteam16") +
                        "&IMEI=" + IMEI +
                        "&UserID=" + sp.getString("MDUserID", ""))
            .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataProject.add(responseArray.getJSONObject(i).getString("NamaProject"));
                        dataProjectID.add(responseArray.getJSONObject(i).getString("Project"));
                    }
                    project = responseArray.getJSONObject(0).getString("Project");
                    FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterProject.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterReportPengadaan.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        spinnerHRDept.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                HRDept = dataHRDeptID.get((int) l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spinnerProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                project = dataProjectID.get((int) l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerTipe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipe = dataTipeID.get((int) id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button buttonSearch = (Button) findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra("dari", dariTanggal);
                i.putExtra("sampai", sampaiTanggal);
                i.putExtra("project", project);
                i.putExtra("tipe", tipe);
                i.putExtra("HRDept", HRDept);
                setResult(RESULT_OK, i);
                finish();
            }
        });

        Fabric.with(this, new Crashlytics());

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Filter Report Pengadaan"));
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_filter_general, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.resetFilter) {
            spinnerHRDept.setSelection(0);
            spinnerProject.setSelection(0);
            spinnerTipe.setSelection(0);
            project = "";
            HRDept = "";
            tipe = "";
            dariTanggal = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1)
                    + "-" + String.valueOf(Calendar.getInstance().getMinimum(Calendar.DAY_OF_MONTH)) + "-"
                    + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            sampaiTanggal = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1)
                    + "-" + String.valueOf(Calendar.getInstance().getMaximum(Calendar.DAY_OF_MONTH)) + "-"
                    + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            textDari.setText(String.valueOf(Calendar.getInstance().getMinimum(Calendar.DAY_OF_MONTH)) + " "
                    + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                    + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
            textSampai.setText(String.valueOf(Calendar.getInstance().getMaximum(Calendar.DAY_OF_MONTH)) + " "
                    + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                    + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
