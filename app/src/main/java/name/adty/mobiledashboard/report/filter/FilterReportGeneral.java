package name.adty.mobiledashboard.report.filter;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static name.adty.mobiledashboard.R.id.spinnerPerusahaan;
import static name.adty.mobiledashboard.R.string.textTahun;

public class FilterReportGeneral extends AppCompatActivity {

    final String TAG = "MDFilterReportNeraca";
    String month;
    String year;
    String perusahaan = "";
    String project = "";
    String tampilan = "0";
    String sifatAkun = "0";
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    String IMEI = "";
    List<String> dataPerusahaan = new ArrayList<String>();
    List<String> dataPerusahaanID = new ArrayList<String>();
    List<String> dataProject = new ArrayList<String>();
    List<String> dataProjectID = new ArrayList<String>();
    String namaBulan[] = new String[]{"Januari", "Februari", "Maret" ,"April", "Mei", "Juni", "Juli",
        "Agustus", "September", "Oktober", "November", "Desember"
    };
    Spinner spinnerPerusahaan = null;
    Spinner spinnerProject = null;
    RadioGroup radioGroupTampilan = null;
    RadioGroup radioGroupSifatAkun = null;
    TextView textBulan;
    TextView textTahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mobileDashboard = new MobileDashboard(getApplicationContext());
        sp = getSharedPreferences("MobileDashboard", MODE_PRIVATE);
        month = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);
        year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        IMEI = mobileDashboard.getIMEI();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_report_general);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final LayoutInflater layoutInflater = getLayoutInflater();

        textBulan = (TextView) findViewById(R.id.textViewBulan);
        textTahun = (TextView) findViewById(R.id.textViewTahun);
        LinearLayout buttonDatePicker = (LinearLayout) findViewById(R.id.buttonDatePicker);

        textBulan.setText(namaBulan[Integer.valueOf(month) - 1]);
        textTahun.setText(year);

        buttonDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dateDialogBuilder = new AlertDialog.Builder(FilterReportGeneral.this);
                dateDialogBuilder.setView(layoutInflater.inflate(R.layout.date_picker_dialog, null));
                final AlertDialog dateDialog = dateDialogBuilder.create();
                dateDialog.show();
                final DatePicker datePicker = (DatePicker) dateDialog.findViewById(R.id.datePicker);
                datePicker.init(Integer.valueOf(year), Integer.valueOf(month), 0, new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {

                    }
                });
                View dayPicker = dateDialog.findViewById(datePicker.getContext().getResources().getIdentifier("android:id/day", null, null));
                dayPicker.setVisibility(View.GONE);
                Button buttonOK = (Button) dateDialog.findViewById(R.id.buttonOK);
                buttonOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        month = String.valueOf(datePicker.getMonth() + 1);
                        year = String.valueOf(datePicker.getYear());
                        textBulan.setText(namaBulan[Integer.valueOf(month) - 1]);
                        textTahun.setText(year);
                        dateDialog.dismiss();
                    }
                });
                Button buttonCancel = (Button) dateDialog.findViewById(R.id.buttonCancel);
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dateDialog.dismiss();
                    }
                });
            }
        });

        final ArrayAdapter<String> adapterPerusahaan = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataPerusahaan);
        adapterPerusahaan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPerusahaan = (Spinner) findViewById(R.id.spinnerPerusahaan);
        spinnerPerusahaan.setAdapter(adapterPerusahaan);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetPerusahaan" +
                        "?Token=" + mobileDashboard.MD5(IMEI + "poteam16") +
                        "&IMEI=" + IMEI +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterReportGeneral.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataPerusahaan.add(responseArray.getJSONObject(i).getString("NamaPers"));
                        dataPerusahaanID.add(responseArray.getJSONObject(i).getString("Pers"));
                    }
                    perusahaan = responseArray.getJSONObject(0).getString("Pers");
                    FilterReportGeneral.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterPerusahaan.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterReportGeneral.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        final ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataProject);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProject = (Spinner) findViewById(R.id.spinnerProject);
        spinnerProject.setAdapter(adapterProject);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetProject" +
                        "?Token=" + mobileDashboard.MD5(IMEI + "poteam16") +
                        "&IMEI=" + IMEI +
                        "&UserID=" + sp.getString("MDUserID", ""))
            .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterReportGeneral.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Error Connection", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataProject.add(responseArray.getJSONObject(i).getString("NamaProject"));
                        dataProjectID.add(responseArray.getJSONObject(i).getString("Project"));
                    }
                    project = responseArray.getJSONObject(0).getString("Project");
                    FilterReportGeneral.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterProject.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterReportGeneral.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        radioGroupTampilan = (RadioGroup) findViewById(R.id.radioGroupTampilan);
        radioGroupTampilan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton rb = (RadioButton) radioGroup.findViewById(i);
                tampilan = String.valueOf(radioGroup.indexOfChild(rb));
            }
        });

        radioGroupSifatAkun = (RadioGroup) findViewById(R.id.radioGroupSifatAkun);
        radioGroupSifatAkun.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton rb = (RadioButton) radioGroup.findViewById(i);
                sifatAkun = String.valueOf(radioGroup.indexOfChild(rb));
            }
        });

        spinnerPerusahaan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                perusahaan = dataPerusahaanID.get((int) l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spinnerProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                project = dataProjectID.get((int) l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button buttonSearch = (Button) findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra("bulan", month);
                i.putExtra("tahun", year);
                i.putExtra("perusahaan", perusahaan);
                i.putExtra("project", project);
                i.putExtra("tampilan", tampilan);
                i.putExtra("sifatAkun", sifatAkun);
                setResult(RESULT_OK, i);
                finish();
            }
        });

        Fabric.with(this, new Crashlytics());

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Filter Report General"));
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
        finish();
    }

    @Nullable
    @Override
    public Intent getSupportParentActivityIntent() {
        Intent parentIntent= getIntent();
        String className = parentIntent.getStringExtra("ParentClassName"); //getting the parent class name

        Intent newIntent=null;
        try {
            //you need to define the class with package name
            newIntent = new Intent(FilterReportGeneral.this,Class.forName("name.adty.mobiledashboard.report."+className));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_filter_general, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.resetFilter) {
            spinnerPerusahaan.setSelection(0);
            spinnerProject.setSelection(0);
            radioGroupSifatAkun.check(R.id.radioSifatAkunSaldo);
            radioGroupTampilan.check(R.id.radioTampilanSummary);
            month = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);
            year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            textBulan.setText(namaBulan[Integer.valueOf(month) - 1]);
            textTahun.setText(year);
            perusahaan = "";
            project = "";
            tampilan = "0";
            sifatAkun = "0";
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
