package name.adty.mobiledashboard.report;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.Login;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.approval.ApprovalMain;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ReportMain extends AppCompatActivity {

    private static final String TAG = "ReportMain";

    private SlidingUpPanelLayout mLayout;
    TextView textUserName;
    TextView textUserRole;
    LinearLayout buttonReport;
    LinearLayout buttonApproval;
    Toolbar mainToolbar;
    MenuInflater menuInflater;
    SharedPreferences sharedPreferences;
    MobileDashboard mobileDashboard;
    ReportAdapter adapter;
    List<ReportItem> items = new ArrayList<ReportItem>();
    ProgressBar progressBar;
    GridView gridView;
    TextView textError;

    @Override
    protected void onResume() {
        super.onResume();
        loadReportMenu();

        Fabric.with(this, new Crashlytics());

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Report Main"));
    }

    public void loadReportMenu() {
        textError.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetListLaporan" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&UserID=" + sharedPreferences.getString("MDUserID", ""))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ReportMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ReportMain.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                        textError.setVisibility(View.VISIBLE);
                        Log.d(TAG, "load report menu fail");
                        e.printStackTrace();
                        Crashlytics.log(Log.DEBUG, TAG, "load report menu fail. user = " + sharedPreferences.getString("MDNamaUser", ""));
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                JSONArray responseJSON = null;
                try {
                    responseJSON = new JSONArray(responseBody);
                    items.clear();
                    for (int i = 0; i < responseJSON.length(); i++) {
                        switch (responseJSON.getString(i)) {
                            case "Laporan Budget Control":
                                items.add(new ReportItem("BUDGET CONTROL", R.mipmap.ic_budget_control));
                                break;
                            case "Laporan Cash Flow":
                                items.add(new ReportItem("CASH FLOW", R.mipmap.ic_cash_flow));
                                break;
                            case "Laporan Keuangan Laba Rugi":
                                items.add(new ReportItem("LABA RUGI", R.mipmap.ic_labarugi));
                                break;
                            case "Laporan Keuangan Neraca":
                                items.add(new ReportItem("NERACA", R.mipmap.ic_neraca));
                                break;
                            case "Laporan Pengadaan":
                                items.add(new ReportItem("PENGADAAN", R.mipmap.ic_pengadaan));
                                break;
                            case "Laporan Penjualan":
                                items.add(new ReportItem("PENJUALAN", R.mipmap.ic_penjualan));
                                break;
                            case "Laporan Piutang":
                                items.add(new ReportItem("AGING PIUTANG", R.mipmap.ic_aging_piutang));
                                break;
                            case "Laporan Rasio Keuangan (ROI, IRR, PBV, PER)":
                                items.add(new ReportItem("RASIO KEUANGAN", R.mipmap.ic_rasio_keuangan));
                                break;
                            case "Laporan Rasio Operasional":
                                items.add(new ReportItem("RASIO OPERASIONAL", R.mipmap.ic_rasio_operational));
                                break;
                        }
                    }
                } catch (final JSONException e) {
                    ReportMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(ReportMain.this, new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                                Log.d(TAG, "log report menu error with value: " + responseBody);
                                e.printStackTrace();
                                Crashlytics.log(Log.DEBUG, TAG, "load report menu error with value. user = " +
                                        sharedPreferences.getString("MDNamaUser", "") + ". value = " + responseBody);
                            } catch (JSONException e1) {
                                Toast.makeText(ReportMain.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                Log.d(TAG, "log report menu error JSON");
                                e1.printStackTrace();
                                Crashlytics.log(Log.DEBUG, TAG, "load report menu error JSON. user = " +
                                        sharedPreferences.getString("MDNamaUser", "") + ". value = " + responseBody);
                            } finally {
                                textError.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } finally {
                    response.body().close();

                    ReportMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mobileDashboard = new MobileDashboard(ReportMain.this);
        sharedPreferences = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);

        textError = (TextView) findViewById(R.id.text_error);

        mainToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mainToolbar.setTitle("Report");
        menuInflater = getMenuInflater();

        buttonApproval = (LinearLayout) findViewById(R.id.buttonApproval);
        buttonReport = (LinearLayout) findViewById(R.id.buttonReport);

        if (!sharedPreferences.getBoolean("MDUserMenuReport", false)) {
            buttonReport.setVisibility(View.GONE);
        }

        if (!sharedPreferences.getBoolean("MDUserMenuApproval", false)) {
            buttonApproval.setVisibility(View.GONE);
        }

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (previousState == SlidingUpPanelLayout.PanelState.ANCHORED && newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.VISIBLE);
                } else if (previousState == SlidingUpPanelLayout.PanelState.COLLAPSED && newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.GONE);
                } else if (newState != SlidingUpPanelLayout.PanelState.COLLAPSED && newState != SlidingUpPanelLayout.PanelState.HIDDEN && newState != SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.GONE);
                } else {
                    mainToolbar.setVisibility(View.VISIBLE);
                }
            }
        });

        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
                } else if (mLayout.getPanelState() != SlidingUpPanelLayout.PanelState.HIDDEN) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            }
        });

        textUserName = (TextView) findViewById(R.id.textUserName);
        textUserName.setText(sharedPreferences.getString("MDNamaUser", ""));
        textUserRole = (TextView) findViewById(R.id.textUserRole);
        textUserRole.setText(sharedPreferences.getString("MDUserRole", ""));

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        gridView = (GridView) findViewById(R.id.gridView);
        adapter = new ReportAdapter(ReportMain.this, items);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView text = (TextView) view.findViewById(R.id.text);
                switch (text.getText().toString()) {
                    case "NERACA":
                        Intent intentNeraca = new Intent(ReportMain.this, ReportNeraca.class);
                        startActivity(intentNeraca);
                        break;
                    case "LABA RUGI":
                        Intent intentLabaRugi = new Intent(ReportMain.this, ReportLabaRugi.class);
                        startActivity(intentLabaRugi);
                        break;
                    case "CASH FLOW":
                        Intent intentCashFlow = new Intent(ReportMain.this, ReportCashFlow.class);
                        startActivity(intentCashFlow);
                        break;
                    case "PENJUALAN":
                        Intent intentPenjualan= new Intent(ReportMain.this, ReportPenjualan.class);
                        startActivity(intentPenjualan);
                        break;
                    case "PENGADAAN":
                        Intent intentPengadaan = new Intent(ReportMain.this, ReportPengadaan.class);
                        startActivity(intentPengadaan);
                        break;
                    case "BUDGET CONTROL":
                        Intent intentBudgetControl = new Intent(ReportMain.this, ReportBudgetControl.class);
                        startActivity(intentBudgetControl);
                        break;
                    case "RASIO KEUANGAN":
                        Intent intentRasioKeuangan = new Intent(ReportMain.this, ReportRasioKeuangan.class);
                        startActivity(intentRasioKeuangan);
                        break;
                    case "AGING PIUTANG":
                        Intent intentAgingPiutang = new Intent(ReportMain.this, ReportAgingPiutang.class);
                        startActivity(intentAgingPiutang);
                        break;
                }
            }
        });
    }

    public void clickButtonLogout(View view) {
        new AlertDialog.Builder(ReportMain.this)
                .setTitle("Logout Confirmation")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new OkHttpClient().newCall(new Request.Builder()
                                .url(mobileDashboard.APIURL + "/Logout" +
                                        "?IMEI=" + mobileDashboard.getIMEI() +
                                        "&Token=" + mobileDashboard.getToken() +
                                        "&FirebaseID=" + sharedPreferences.getString("firebaseToken", "") +
                                        "&UserID=" + sharedPreferences.getString("MDUserID", ""))
                                .method("POST", RequestBody.create(null, new byte[0]))
                                .build()
                        ).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, final IOException e) {
                                ReportMain.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(ReportMain.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                String responseBody = response.body().string();
                                JSONObject object = null;
                                try {
                                    object = new JSONObject(responseBody);
                                    if (object.getString("Value").equals("1")) {
                                        Answers.getInstance().logCustom(new CustomEvent("Logout"));

                                        SharedPreferences.Editor spe = sharedPreferences.edit();
                                        spe.putString("MobileDashboardLogin", "");
                                        spe.clear();
                                        spe.commit();
                                        startActivity(new Intent(ReportMain.this, Login.class));
                                        finish();
                                    }
                                } catch (final JSONException e) {
                                    ReportMain.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ReportMain.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("No", null).show();
    }

    public void clickButtonReport(View view) {
        Intent intent = new Intent(ReportMain.this, ReportMain.class);
        startActivity(intent);
        finish();
    }

    public void clickButtonApproval(View view) {
        SharedPreferences.Editor spe = sharedPreferences.edit();
        spe.putString("ApprovalView", "approval");
        spe.commit();

        Intent intent = new Intent(ReportMain.this, ApprovalMain.class);
        startActivity(intent);
        finish();
    }

    public void clickUpDrawer(View view) {
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }
}