package name.adty.mobiledashboard.report;

/**
 * Created by KLOPOS on 11/29/2016.
 */

public class ReportNeracaItem {
    String link;
    String nama;

    public ReportNeracaItem() {
    }

    public ReportNeracaItem(String link, String nama) {
        this.link = link;
        this.nama = nama;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
