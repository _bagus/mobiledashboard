package name.adty.mobiledashboard.report;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by KLOPOS on 11/22/2016.
 */

public class ReportAdapter extends BaseAdapter {
    private List<ReportItem> mItems = new ArrayList<ReportItem>();
    private final LayoutInflater mInflater;

    public ReportAdapter(final Context context, List<ReportItem> reportItem) {
        mInflater = LayoutInflater.from(context);
        mItems = reportItem;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public ReportItem getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mItems.get(i).drawableId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name;

        if (v == null) {
            v = mInflater.inflate(R.layout.report_item_grid, viewGroup, false);
            v.setTag(R.id.image, v.findViewById(R.id.image));
            v.setTag(R.id.text, v.findViewById(R.id.text));
        }

        picture = (ImageView) v.getTag(R.id.image);
        name = (TextView) v.getTag(R.id.text);

        ReportItem item = getItem(i);

        picture.setImageResource(item.drawableId);
        name.setText(item.name);

        return v;
    }
}
