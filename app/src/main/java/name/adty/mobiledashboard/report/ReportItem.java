package name.adty.mobiledashboard.report;

/**
 * Created by KLOPOS on 11/29/2016.
 */

public class ReportItem {
    public final String name;
    public final int drawableId;

    ReportItem(String name, int drawableId) {
        this.name = name;
        this.drawableId = drawableId;
    }
}
