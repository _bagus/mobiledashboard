package name.adty.mobiledashboard.report.filter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FilterReportCashFlow extends AppCompatActivity {

    final String TAG = "MDFilterReportCashFlow";
    String dariTanggal;
    String sampaiTanggal;
    String perusahaan = "";
    String project = "";
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    String IMEI = "";
    List<String> dataPerusahaan = new ArrayList<String>();
    List<String> dataPerusahaanID = new ArrayList<String>();
    List<String> dataProject = new ArrayList<String>();
    List<String> dataProjectID = new ArrayList<String>();

    TextView textDari;
    TextView textSampai;
    Spinner spinnerPerusahaan;
    Spinner spinnerProject;

    static final String namaBulan[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mobileDashboard = new MobileDashboard(getApplicationContext());
        sp = getSharedPreferences("MobileDashboard", MODE_PRIVATE);
        IMEI = mobileDashboard.getIMEI();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_report_cash_flow);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final LayoutInflater layoutInflater = getLayoutInflater();

        LinearLayout buttonDatePicker = (LinearLayout) findViewById(R.id.buttonDatePicker);
        LinearLayout buttonDatePicker2 = (LinearLayout) findViewById(R.id.buttonDatePicker2);

        dariTanggal = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1)
                + "-" + String.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)) + "-"
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        sampaiTanggal = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1)
                + "-" + String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) + "-"
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        textDari = (TextView) findViewById(R.id.textDari);
        textDari.setText(String.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)) + " "
                + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
        textSampai = (TextView) findViewById(R.id.textSampai);
        textSampai.setText(String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) + " "
                + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));

        buttonDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dpd = new DatePickerDialog(FilterReportCashFlow.this, R.style.customDatePickerDialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                dariTanggal = String.valueOf(monthOfYear + 1)
                                        + "-" + String.valueOf(dayOfMonth) + "-"
                                        + String.valueOf(year);
                                textDari.setText(String.valueOf(dayOfMonth) + " "
                                        + namaBulan[monthOfYear] + " " + String.valueOf(year));
                            }
                        },
                        Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR)),
                        Integer.valueOf(Calendar.getInstance().get(Calendar.MONTH)),
                        Integer.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH))
                );

                dpd.show();
            }
        });

        buttonDatePicker2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(FilterReportCashFlow.this, R.style.customDatePickerDialog,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                sampaiTanggal = String.valueOf(monthOfYear + 1)
                                        + "-" + String.valueOf(dayOfMonth) + "-"
                                        + String.valueOf(year);
                                textSampai.setText(String.valueOf(dayOfMonth) + " "
                                        + namaBulan[monthOfYear] + " " + String.valueOf(year));
                            }
                        },
                        Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR)),
                        Integer.valueOf(Calendar.getInstance().get(Calendar.MONTH)),
                        Integer.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH))
                );

                dpd.show();
            }
        });

        final ArrayAdapter<String> adapterPerusahaan = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataPerusahaan);
        adapterPerusahaan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPerusahaan = (Spinner) findViewById(R.id.spinnerPerusahaan);
        spinnerPerusahaan.setAdapter(adapterPerusahaan);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetPerusahaan" +
                        "?Token=" + mobileDashboard.MD5(IMEI + "poteam16") +
                        "&IMEI=" + IMEI +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterReportCashFlow.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataPerusahaan.add(responseArray.getJSONObject(i).getString("NamaPers"));
                        dataPerusahaanID.add(responseArray.getJSONObject(i).getString("Pers"));
                    }
                    perusahaan = responseArray.getJSONObject(0).getString("Pers");
                    FilterReportCashFlow.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterPerusahaan.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterReportCashFlow.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Respose Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        final ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataProject);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProject = (Spinner) findViewById(R.id.spinnerProject);
        spinnerProject.setAdapter(adapterProject);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetProject" +
                        "?Token=" + mobileDashboard.MD5(IMEI + "poteam16") +
                        "&IMEI=" + IMEI +
                        "&UserID=" + sp.getString("MDUserID", ""))
            .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterReportCashFlow.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataProject.add(responseArray.getJSONObject(i).getString("NamaProject"));
                        dataProjectID.add(responseArray.getJSONObject(i).getString("Project"));
                    }
                    project = responseArray.getJSONObject(0).getString("Project");
                    FilterReportCashFlow.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterProject.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterReportCashFlow.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        spinnerPerusahaan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                perusahaan = dataPerusahaanID.get((int) l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spinnerProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                project = dataProjectID.get((int) l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button buttonSearch = (Button) findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra("dari", dariTanggal);
                i.putExtra("sampai", sampaiTanggal);
                i.putExtra("perusahaan", perusahaan);
                i.putExtra("project", project);
                setResult(RESULT_OK, i);
                finish();
            }
        });

        Fabric.with(this, new Crashlytics());

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Filter Report Cash Flow"));
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
        finish();
    }

    @Nullable
    @Override
    public Intent getSupportParentActivityIntent() {
        Intent parentIntent= getIntent();
        String className = parentIntent.getStringExtra("ParentClassName"); //getting the parent class name

        Intent newIntent=null;
        try {
            //you need to define the class with package name
            newIntent = new Intent(FilterReportCashFlow.this,Class.forName("name.adty.mobiledashboard.report."+className));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_filter_general, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.resetFilter) {
            perusahaan = "";
            project = "";
            spinnerPerusahaan.setSelection(0);
            spinnerProject.setSelection(0);
            dariTanggal = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1)
                    + "-" + String.valueOf(Calendar.getInstance().getMinimum(Calendar.DAY_OF_MONTH)) + "-"
                    + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            sampaiTanggal = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1)
                    + "-" + String.valueOf(Calendar.getInstance().getMaximum(Calendar.DAY_OF_MONTH)) + "-"
                    + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            textDari.setText(String.valueOf(Calendar.getInstance().getMinimum(Calendar.DAY_OF_MONTH)) + " "
                    + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                    + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
            textSampai.setText(String.valueOf(Calendar.getInstance().getMaximum(Calendar.DAY_OF_MONTH)) + " "
                    + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                    + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
