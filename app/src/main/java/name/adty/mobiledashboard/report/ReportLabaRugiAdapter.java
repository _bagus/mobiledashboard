package name.adty.mobiledashboard.report;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import name.adty.mobiledashboard.R;

/**
 * Created by KLOPOS on 11/29/2016.
 */

public class ReportLabaRugiAdapter extends BaseAdapter {

    private Context context;
    private List<ReportLabaRugiItem> items = new ArrayList<ReportLabaRugiItem>();
    private final LayoutInflater layoutInflater;

    public ReportLabaRugiAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    public ReportLabaRugiAdapter(Context context, List<ReportLabaRugiItem> items) {
        this.layoutInflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ReportLabaRugiItem getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView name;
        ImageView image;

        if (v == null) {
            v = layoutInflater.inflate(R.layout.report_neraca_item, viewGroup, false);
            v.setTag(R.id.text, v.findViewById(R.id.text));
            v.setTag(R.id.image, v.findViewById(R.id.image));
        }

        name = (TextView) v.getTag(R.id.text);
        image = (ImageView) v.getTag(R.id.image);
        ReportLabaRugiItem item = getItem(i);
        name.setText(item.getNama());
        image.setImageResource(R.mipmap.ic_download);

        return v;
    }
}
