package name.adty.mobiledashboard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.approval.ApprovalMain;
import name.adty.mobiledashboard.global.MobileDashboard;
import name.adty.mobiledashboard.report.ReportMain;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Main extends AppCompatActivity {

    private static final String TAG = "Main";

    private SlidingUpPanelLayout mLayout;
    TextView textUserName;
    TextView textUserName2;
    TextView textUserRole;
    TextView textUserRole2;
    LinearLayout buttonReport;
    LinearLayout buttonApproval;
    LinearLayout layoutButtonReport;
    LinearLayout layoutButtonApproval;
    Toolbar mainToolbar;
    MenuInflater menuInflater;
    public static final int FILTER_SPBJ = 0;
    SharedPreferences sharedPreferences;
    MobileDashboard mobileDashboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mobileDashboard = new MobileDashboard(Main.this);
        sharedPreferences = getSharedPreferences("MobileDashboard",Context.MODE_PRIVATE);

        mainToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mainToolbar.setTitle("HK Realtindo");
        menuInflater = getMenuInflater();

        buttonApproval = (LinearLayout) findViewById(R.id.buttonApproval);
        buttonReport = (LinearLayout) findViewById(R.id.buttonReport);
        layoutButtonReport = (LinearLayout) findViewById(R.id.layoutButtonReport);
        layoutButtonApproval= (LinearLayout) findViewById(R.id.layoutButtonApproval);

        if (!sharedPreferences.getBoolean("MDUserMenuReport", false)) {
            buttonReport.setVisibility(View.GONE);
            layoutButtonReport.setVisibility(View.GONE);
        }

        if (!sharedPreferences.getBoolean("MDUserMenuApproval", false)) {
            buttonApproval.setVisibility(View.GONE);
            layoutButtonApproval.setVisibility(View.GONE);
        }

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (previousState == SlidingUpPanelLayout.PanelState.ANCHORED && newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.VISIBLE);
                } else if (previousState == SlidingUpPanelLayout.PanelState.COLLAPSED && newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.GONE);
                } else if (newState != SlidingUpPanelLayout.PanelState.COLLAPSED && newState != SlidingUpPanelLayout.PanelState.HIDDEN && newState != SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.GONE);
                } else {
                    mainToolbar.setVisibility(View.VISIBLE);
                }
            }
        });

        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
                } else if(mLayout.getPanelState() != SlidingUpPanelLayout.PanelState.HIDDEN) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            }
        });

        sharedPreferences = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        textUserName = (TextView) findViewById(R.id.textUserName);
        textUserName2 = (TextView) findViewById(R.id.textUserName2);
        textUserName.setText(sharedPreferences.getString("MDNamaUser", ""));
        textUserName2.setText(sharedPreferences.getString("MDNamaUser", ""));
        textUserRole = (TextView) findViewById(R.id.textUserRole);
        textUserRole2 = (TextView) findViewById(R.id.textUserRole2);
        textUserRole.setText(sharedPreferences.getString("MDUserRole", ""));
        textUserRole2.setText(sharedPreferences.getString("MDUserRole", ""));

        Fabric.with(this, new Crashlytics());
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Main"));

    }

    public void clickButtonLogout(View view) {
        new AlertDialog.Builder(Main.this)
                .setTitle("Logout Confirmation")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new OkHttpClient().newCall(new Request.Builder()
                                .url(mobileDashboard.APIURL +"/Logout" +
                                        "?UserID=" + sharedPreferences.getString("MDUserID", "") +
                                        "&IMEI=" + mobileDashboard.getIMEI() +
                                        "&Token=" + mobileDashboard.getToken() +
                                        "&FirebaseID=" + sharedPreferences.getString("firebaseToken", ""))
                                .method("POST", RequestBody.create(null, new byte[0]))
                                .build()
                        ).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, final IOException e) {
                                Main.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(Main.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                String responseBody = response.body().string();
                                JSONObject object = null;
                                try {
                                    object = new JSONObject(responseBody);
                                    if (object.getString("Value").equals("1")) {
                                        Answers.getInstance().logCustom(new CustomEvent("Logout"));

                                        SharedPreferences.Editor spe = sharedPreferences.edit();
                                        spe.putString("MobileDashboardLogin", "");
                                        spe.clear();
                                        spe.commit();
                                        startActivity(new Intent(Main.this, Login.class));
                                        finish();
                                    }
                                } catch (final JSONException e) {
                                    Main.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(Main.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("No", null).show();
    }

    public void clickButtonReport(View view) {
        Intent intent = new Intent(Main.this, ReportMain.class);
        startActivity(intent);
        finish();
    }

    public void clickButtonApproval(View view) {
        SharedPreferences.Editor spe = sharedPreferences.edit();
        spe.putString("ApprovalView", "approval");
        spe.commit();

        Intent intent = new Intent(Main.this, ApprovalMain.class);
        startActivity(intent);
        finish();
    }

    public void clickUpDrawer(View view) {
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
