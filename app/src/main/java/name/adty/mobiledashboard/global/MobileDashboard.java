package name.adty.mobiledashboard.global;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.compat.BuildConfig;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by KLOPOS on 11/28/2016.
 */

public class MobileDashboard {
    Context context;
    public final static String APIURL = "http://103.77.106.10:7002/api";
    SharedPreferences sp;
    SharedPreferences.Editor spe;

    public MobileDashboard() {
    }

    public MobileDashboard(Context context) {
        this.context = context;
        sp = context.getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        spe = sp.edit();
    }

    public String getIMEI()
    {
        String IMEI = "";
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        IMEI = tm.getDeviceId();
        if(IMEI.equals("000000000000000")) {
            IMEI = "A100003B8E601C5";
        } else if (IMEI.equals("A10000421FD4CA")) {
            IMEI = "868236020788338";
        }

        try {
            if ((context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
                switch (sp.getString("username", "")) {
                    case "POTEAMA":
                        IMEI = "355684041204977";
                        break;
                    case "POTEAMB":
                        IMEI = "862709026074214";
                        break;
                    case "POTEAMC":
                        IMEI = "A100003B8E601C5";
                        break;
                    case "POTEAMD":
                        IMEI = "860071026417154";
                        break;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return IMEI;
    }

    public String MD5(String password) {
        try {

            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getToken() {
        return MD5(getIMEI() + "poteam16");
    }

    public void openDownloadedFile(String name) {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    + "/" + name);//name here is the name of any string you want to pass to the method
            if (!file.isDirectory())
                file.mkdir();
            Intent testIntent = new Intent("com.adobe.reader");
            testIntent.setType("application/pdf");
            testIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            testIntent.setAction(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            testIntent.setDataAndType(uri, "application/pdf");
            context.startActivity(testIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getNotifID(String SPBJID) {

        String arrSPBJID[] = SPBJID.split("/");
        int jumlah = Integer.valueOf(arrSPBJID[1]) + Integer.valueOf(arrSPBJID[3]);
        SPBJID = String.valueOf(jumlah) + arrSPBJID[2];
        StringBuilder sb = new StringBuilder();
//        String ascString = null;
        int asciiInt = 0;
        for (int i = 0; i < SPBJID.length(); i++){
            sb.append((int) SPBJID.charAt(i));
            char c = SPBJID.charAt(i);
            asciiInt = (int) SPBJID.charAt(i);
        }
//        ascString = sb.toString();
//        asciiInt = Integer.valueOf(ascString);
        return asciiInt;
    }
}
