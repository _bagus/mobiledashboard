package name.adty.mobiledashboard.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by adty on 14/02/17.
 */

public class MobileDashboardPushInstanceService extends FirebaseInstanceIdService {
    private static final String TAG = "MDPIS";

    private MobileDashboard mobileDashboard;
    private SharedPreferences sp;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        mobileDashboard = new MobileDashboard(MobileDashboardPushInstanceService.this);
        sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);

        final String token = FirebaseInstanceId.getInstance().getToken();

        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/Login" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&UserID=" + sp.getString("MDUserID", "") +
                        "&Password=" + sp.getString("MDUserPassword", "") +
                        "&FirebaseID=" + token)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseBody = response.body().string();
                JSONObject data = null;
                try {
                    data = new JSONObject(responseBody);
                    if (data.getString("Value").equals("1")) {
                        SharedPreferences.Editor spe = sp.edit();
                        spe.putString("firebaseToken", token);
                        spe.commit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Log.d(TAG, "firebaseToken: " + token);

        Fabric.with(this, new Crashlytics());
    }
}
