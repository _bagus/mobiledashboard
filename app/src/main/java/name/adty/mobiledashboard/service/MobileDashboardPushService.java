package name.adty.mobiledashboard.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.approval.ApprovalDetailTab;
import name.adty.mobiledashboard.approval.ApprovalForum;
import name.adty.mobiledashboard.approval.ApprovalNotification;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by adty on 14/02/17.
 */

public class MobileDashboardPushService extends FirebaseMessagingService {
    private static final String TAG = "MDPS";
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    SharedPreferences.Editor spe;

    @Override
    public void onCreate() {
        super.onCreate();
        mobileDashboard = new MobileDashboard(MobileDashboardPushService.this);
        Fabric.with(this, new Crashlytics());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        spe = sp.edit();

        Intent intent;

        Map<String, String> data = remoteMessage.getData();

        String notificationTag = "";

        if (data.get("Approval").toString().equals("true")) {
            if (data.get("Approve").equals("true")) {
                intent = new Intent(MobileDashboardPushService.this, ApprovalDetailTab.class);
                spe.putString("ApprovalView", "approval");
                spe.putString("ApprovalDetailType", "approval");
                notificationTag = "MD_APPROVAL";
            } else {
                intent = new Intent(MobileDashboardPushService.this, ApprovalDetailTab.class);
                spe.putString("ApprovalView", "spbj");
                spe.putString("ApprovalDetailType", "spbj");
                notificationTag = "MD_SPBJ";
            }
        } else {
            intent = new Intent(MobileDashboardPushService.this, ApprovalForum.class);
            intent.putExtra("SPBJID", data.get("Pesan").toString());
            spe.putString("ApprovalView", "spbj");
            spe.putString("ApprovalDetailType", "spbj");
            notificationTag = "MD_FORUM";
        }

        String SPBJID = data.get("Pesan").toString();
        int NotifID = mobileDashboard.getNotifID(SPBJID);
        intent.putExtra("NotificationClick", true);
        intent.putExtra("NotificationIDPesan", data.get("IDPesan").toString());

        spe.putString("ApprovalDetailSPBJID", data.get("Pesan").toString());
        spe.putString("NotificationTAG", notificationTag);
        spe.putInt("NotificationID", NotifID);
        spe.putString("NotificationIDPesan", data.get("IDPesan").toString());
        spe.commit();

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(MobileDashboardPushService.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mNotificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(remoteMessage.getData().get("title"))
                .setContentText(remoteMessage.getData().get("body"))
                .setAutoCancel(true)
                .setColor(this.getResources().getColor(R.color.MDprimary2))
                .setSound(notificationSoundURI)
                .setContentIntent(pIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationTag, NotifID, mNotificationBuilder.build());
    }
}
