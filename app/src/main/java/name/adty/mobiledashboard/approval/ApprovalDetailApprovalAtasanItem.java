package name.adty.mobiledashboard.approval;

/**
 * Created by adty on 24/01/17.
 */

public class ApprovalDetailApprovalAtasanItem {
    public final String tanggal;
    public final String oleh;
    public final String note;
    public final String status;

    ApprovalDetailApprovalAtasanItem(String tanggal, String oleh, String note, String status) {
        this.tanggal= tanggal;
        this.oleh = oleh;
        this.note = note;
        this.status = status;
    }
}
