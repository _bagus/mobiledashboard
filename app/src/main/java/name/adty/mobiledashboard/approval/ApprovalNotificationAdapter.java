package name.adty.mobiledashboard.approval;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import name.adty.mobiledashboard.R;

/**
 * Created by adty on 01/03/17.
 */

public class ApprovalNotificationAdapter extends BaseAdapter {

    private List<ApprovalNotificationItem> items = new ArrayList<ApprovalNotificationItem>();
    private LayoutInflater layoutInflater;
    private SharedPreferences sp;

    public ApprovalNotificationAdapter(Context context, List<ApprovalNotificationItem> items) {
        sp = context.getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        this.items = items;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).idPesan;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.approval_notification_item, parent, false);
        TextView keterangan = (TextView) view.findViewById(R.id.textKeterangan);
        TextView tanggal = (TextView) view.findViewById(R.id.textTanggal);
        LinearLayout container = (LinearLayout) view.findViewById(R.id.container);

        ApprovalNotificationItem item = items.get(position);

        keterangan.setText(item.keterangan);
        tanggal.setText(item.tanggal);

        if (item.baca == 0) {
            container.setBackgroundColor(Color.parseColor("#508CC63F"));
        }

        return view;
    }
}
