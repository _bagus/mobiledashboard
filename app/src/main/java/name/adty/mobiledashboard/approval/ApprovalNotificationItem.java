package name.adty.mobiledashboard.approval;

/**
 * Created by adty on 01/03/17.
 */

public class ApprovalNotificationItem {

    int idPesan;
    boolean approval;
    String tanggal;
    String sender;
    String keterangan;
    String spbjId;
    int baca;
    String approve;

    public ApprovalNotificationItem(int idPesan, boolean approval, String tanggal, String sender,
                                    String keterangan, String spbjId, int baca, String approve) {
        this.idPesan = idPesan;
        this.approval = approval;
        this.tanggal = tanggal;
        this.sender = sender;
        this.keterangan = keterangan;
        this.spbjId = spbjId;
        this.baca = baca;
        this.approve = approve;
    }

}
