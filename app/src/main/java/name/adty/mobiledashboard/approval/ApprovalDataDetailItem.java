package name.adty.mobiledashboard.approval;

import java.util.List;

/**
 * Created by adty on 24/01/17.
 */

public class ApprovalDataDetailItem {
    public final String name;
    public final String keterangan;
    public final Integer jumlah;
    public final Long hargaSatuan;
    public final Long hargaTotal;
    public final String itemID;
    public final String satuan;
    public final String budgetID;
    public final String budget;
    public final String spbjid;
    public final List<String> databudgetName;
    public final List<String> databudgetID;

    ApprovalDataDetailItem(String spbjid, String name, String keterangan, Integer jumlah, Long hargaSatuan,
                           Long hargaTotal, String itemID, String satuan, String budgetID,
                           String budget, List<String> databudgetName, List<String> databudgetID) {
        this.name = name;
        this.keterangan = keterangan;
        this.jumlah = jumlah;
        this.hargaSatuan = hargaSatuan;
        this.hargaTotal = hargaTotal;
        this.itemID = itemID;
        this.satuan = satuan;
        this.budget = budget;
        this.budgetID = budgetID;
        this.spbjid = spbjid;
        this.databudgetID = databudgetID;
        this.databudgetName = databudgetName;
    }
}
