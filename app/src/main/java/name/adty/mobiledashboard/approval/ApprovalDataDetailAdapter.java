package name.adty.mobiledashboard.approval;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by adty on 24/01/17.
 */

public class ApprovalDataDetailAdapter extends BaseAdapter {

    private List<ApprovalDataDetailItem> items = new ArrayList<ApprovalDataDetailItem>();
    private final LayoutInflater layoutInflater;
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    Context context;

    public ApprovalDataDetailAdapter(final Context context, List<ApprovalDataDetailItem> items) {
        layoutInflater = LayoutInflater.from(context);
        this.items = items;
        mobileDashboard = new MobileDashboard(context);
        sp = context.getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ApprovalDataDetailItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v =  convertView;
        final ViewHolder holder;

        if (v == null) {

            holder = new ViewHolder();

            v = layoutInflater.inflate(R.layout.item_approval_detail_spbj, parent, false);

            holder.name = (TextView) v.findViewById(R.id.textItem);
            holder.keterangan = (TextView) v.findViewById(R.id.textKeterangan);
            holder.jumlah= (TextView) v.findViewById(R.id.textJumlah);
            holder.harga = (TextView) v.findViewById(R.id.textHarga);
            holder.editContainer = (LinearLayout) v.findViewById(R.id.editContainer);
            holder.editJumlah = (EditText) v.findViewById(R.id.editJumlah);
            holder.satuanJumlah = (TextView) v.findViewById(R.id.satuanJumlah);
            holder.spinnerBudget = (Spinner) v.findViewById(R.id.spinnerBudget);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        final ApprovalDataDetailItem item = getItem(position);
        final String spbjid = sp.getString("ApprovalDetailSPBJID", "");

        holder.name.setText(item.name);
        holder.keterangan.setText(item.keterangan);
        holder.jumlah.setText(item.jumlah + " " + item.satuan);
        holder.harga.setText(holder.jumlah.getText() + " x " + NumberFormat.getInstance(Locale.US).format(item.hargaSatuan).replace(",", ".").toString() +
                " = " + NumberFormat.getInstance(Locale.US).format(item.hargaTotal).replace(",", "."));

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, item.databudgetName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        holder.satuanJumlah.setText(item.satuan);
        holder.spinnerBudget.setAdapter(adapter);
        holder.spinnerBudget.setSelection(item.databudgetID.indexOf(sp.getString("BudgetForItemSPBJ" + spbjid + item.itemID, item.budgetID)));
        holder.spinnerBudget.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!item.itemID.isEmpty()) {
                    SharedPreferences.Editor spe = sp.edit();
                    spe.putString("BudgetForItemSPBJ" + spbjid + item.itemID, item.databudgetID.get(position));
                    spe.commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.editJumlah.setSingleLine(true);
        holder.editJumlah.setText(sp.getString("QuantityForItemSPBJ" + spbjid + item.itemID, String.valueOf(item.jumlah)));

        holder.editJumlah.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (!item.itemID.isEmpty()) {
                        SharedPreferences.Editor spe = sp.edit();
                        spe.putString("QuantityForItemSPBJ" + spbjid + item.itemID, v.getText().toString().isEmpty() ? "0" : v.getText().toString());
                        spe.commit();
                    }
                }

                return false;
            }
        });

        holder.editJumlah.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!item.itemID.isEmpty()) {
                    SharedPreferences.Editor spe = sp.edit();
                    spe.putString("QuantityForItemSPBJ" + spbjid + item.itemID, s.toString().isEmpty() ? "0" : s.toString());
                    spe.commit();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (sp.getString("ApprovalDetailType", "spbj").equals("approval")) {
            if (sp.getString("MDUserJabatan", "Atasan").equals("Atasan")) {
                holder.editContainer.setVisibility(View.GONE);
                holder.spinnerBudget.setVisibility(View.GONE);
            } else {
                holder.jumlah.setVisibility(View.GONE);
            }
        } else {
            holder.editContainer.setVisibility(View.GONE);
            holder.spinnerBudget.setVisibility(View.GONE);
        }

        return v;
    }

    private class ViewHolder {
        TextView name;
        TextView keterangan;
        TextView jumlah;
        TextView harga;
        LinearLayout editContainer;
        EditText editJumlah;
        TextView satuanJumlah;
        Spinner spinnerBudget;
        int ref;
    }

}
