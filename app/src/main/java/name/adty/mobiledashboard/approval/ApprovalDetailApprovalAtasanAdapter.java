package name.adty.mobiledashboard.approval;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import name.adty.mobiledashboard.R;

/**
 * Created by adty on 24/01/17.
 */

public class ApprovalDetailApprovalAtasanAdapter extends BaseAdapter {

    private List<ApprovalDetailApprovalAtasanItem> items = new ArrayList<ApprovalDetailApprovalAtasanItem>();
    private final LayoutInflater layoutInflater;

    public ApprovalDetailApprovalAtasanAdapter(final Context context, List<ApprovalDetailApprovalAtasanItem> items) {
        layoutInflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ApprovalDetailApprovalAtasanItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v =  convertView;
        TextView tanggal;
        TextView oleh;
        TextView note;
        TextView labelTanggal;

        if (v == null) {
            v = layoutInflater.inflate(R.layout.item_approval_detail_atasan, parent, false);
            v.setTag(R.id.textTanggal, v.findViewById(R.id.textTanggal));
            v.setTag(R.id.labelTanggal, v.findViewById(R.id.labelTanggal));
            v.setTag(R.id.textOleh, v.findViewById(R.id.textOleh));
            v.setTag(R.id.textNote, v.findViewById(R.id.textNote));
        }

        ApprovalDetailApprovalAtasanItem item = getItem(position);

        tanggal = (TextView) v.getTag(R.id.textTanggal);
        tanggal.setText(item.tanggal);

        labelTanggal = (TextView) v.getTag(R.id.labelTanggal);
        if (item.status.toLowerCase().equals("not approved")) {
            labelTanggal.setText("TGL REJECT");
        }

        oleh = (TextView) v.getTag(R.id.textOleh);
        oleh.setText(item.oleh);

        note = (TextView) v.getTag(R.id.textNote);
        note.setText(item.note);

        return v;
    }
}
