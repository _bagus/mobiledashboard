package name.adty.mobiledashboard.approval;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import name.adty.mobiledashboard.R;

/**
 * Created by adty on 24/01/17.
 */

public class ApprovalForumAdapter extends BaseAdapter {

    private List<ApprovalForumItem> items = new ArrayList<ApprovalForumItem>();
    private final LayoutInflater layoutInflater;
    private SharedPreferences sp;

    public ApprovalForumAdapter(final Context context, List<ApprovalForumItem> items) {
        layoutInflater = LayoutInflater.from(context);
        this.items = items;
        sp = context.getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ApprovalForumItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView tanggal;
        TextView user;
        TextView message;

        ApprovalForumItem item = getItem(position);

        if (sp.getString("MDUserID", "").equals(item.user)) {
            v = layoutInflater.inflate(R.layout.forum_item_right, parent, false);
        } else {
            v = layoutInflater.inflate(R.layout.forum_item_left, parent, false);
        }

        String formattedDate = "";
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("d MMM yyyy, HH:mm");
        try {
            Date date = format.parse(item.tanggal);
            formattedDate = format2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tanggal = (TextView) v.findViewById(R.id.tanggal);
        tanggal.setText(formattedDate);

        user = (TextView) v.findViewById(R.id.name);
        user.setText(item.user);

        message = (TextView) v.findViewById(R.id.message);
        message.setText(item.message);

        return v;
    }
}
