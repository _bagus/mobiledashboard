package name.adty.mobiledashboard.approval;

/**
 * Created by adty on 09/02/17.
 */

public class ApprovalForumItem {
    String user;
    String message;
    String tanggal;

    public ApprovalForumItem(String user, String message, String tanggal) {
        this.user = user;
        this.message = message;
        this.tanggal = tanggal;
    }
}
