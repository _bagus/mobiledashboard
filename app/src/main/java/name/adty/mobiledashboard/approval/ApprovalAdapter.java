package name.adty.mobiledashboard.approval;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import name.adty.mobiledashboard.R;

/**
 * Created by adty on 24/01/17.
 */

public class ApprovalAdapter extends BaseAdapter {

    private List<ApprovalItem> items = new ArrayList<ApprovalItem>();
    private final LayoutInflater layoutInflater;

    String[] bulan = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};

    public ApprovalAdapter(final Context context, List<ApprovalItem> items) {
        layoutInflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ApprovalItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).drawableId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v =  convertView;
        ImageView picture;
        TextView name;
        TextView judul;
        TextView oleh;
        TextView tanggal1;
        TextView tanggal2;
        TextView status;
        TextView statusSPBJ;

        if (v == null) {
            v = layoutInflater.inflate(R.layout.approval_item_grid, parent, false);
            v.setTag(R.id.icon, v.findViewById(R.id.icon));
            v.setTag(R.id.nomor, v.findViewById(R.id.nomor));
            v.setTag(R.id.judul, v.findViewById(R.id.judul));
            v.setTag(R.id.tanggal1, v.findViewById(R.id.tanggal1));
            v.setTag(R.id.tanggal2, v.findViewById(R.id.tanggal2));
            v.setTag(R.id.oleh, v.findViewById(R.id.oleh));
            v.setTag(R.id.status, v.findViewById(R.id.status));
            v.setTag(R.id.statusSPBJ, v.findViewById(R.id.statusSPBJ));
        }

        if (items.size() > 0 && position < items.size()) {
            ApprovalItem item = getItem(position);

            picture = (ImageView) v.findViewById(R.id.icon);
            picture.setImageResource(item.drawableId);

            name = (TextView) v.getTag(R.id.nomor);
            name.setText(item.name);

            judul = (TextView) v.getTag(R.id.judul);
            judul.setText(item.judul);

            oleh = (TextView) v.getTag(R.id.oleh);
            oleh.setText("oleh " + item.oleh);

            String[] splitTanggal = item.tanggal1.split(" ");
            String[] splitTanggal1 = splitTanggal[0].split("-");

            tanggal1 = (TextView) v.getTag(R.id.tanggal1);
            tanggal1.setText("Pada tanggal " + splitTanggal1[0] + " "
                    + bulan[Integer.valueOf(splitTanggal1[1]) - 1] + " " + splitTanggal1[2]);

            tanggal2 = (TextView) v.getTag(R.id.tanggal2);
            Date serverDate = null;
            Date deviceDate = null;
            Calendar calendar = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            try {
                serverDate = df.parse(item.tanggal1);
                deviceDate = calendar.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long dateDiff = deviceDate.getTime() - serverDate.getTime();
            long diffInSec = dateDiff / 1000;
            long diffInMin = dateDiff / (60 * 1000);
            long diffInHour = dateDiff / (60 * 60 * 1000);
            long diffInDay = dateDiff / (24 * 60 * 60 * 1000);
            long diffInWeek = dateDiff / (7 * 24 * 60 * 60 * 1000);

            String timeDiff = "";
            boolean minggu = false;
            boolean hari = false;
            boolean jam = false;
            boolean menit = false;
            boolean detik = false;
            if (diffInWeek > 0) {
                minggu = true;
                timeDiff += String.valueOf(diffInWeek) + " minggu ";
            }
            if (diffInDay > 0 && minggu == false) {
                hari = true;
                timeDiff += String.valueOf(diffInDay) + " hari ";
            }
            if (diffInHour > 0 && hari == false && minggu == false) {
                jam = true;
                timeDiff += String.valueOf(diffInHour) + " jam ";
            }
            if (diffInMin > 0 && hari == false && jam == false && minggu == false) {
                menit = true;
                timeDiff += String.valueOf(diffInMin) + " menit ";
            }
            if (diffInSec > 0 && hari == false && jam == false && minggu == false) {
                detik = true;
                timeDiff += String.valueOf(diffInSec) + "detik";
            }
            timeDiff = timeDiff.trim();

            if (!timeDiff.isEmpty()) {
                tanggal2.setText(timeDiff + " yang lalu");
            } else {
                tanggal2.setVisibility(View.GONE);
            }

            status = (TextView) v.getTag(R.id.status);
            status.setText(item.status);
            if (item.status.equals("Normal")) {
                status.setBackgroundResource(R.drawable.spbj_status_normal);
            } else {
                status.setBackgroundResource(R.drawable.spbj_status_urgent);
            }

            statusSPBJ = (TextView) v.getTag(R.id.statusSPBJ);
            statusSPBJ.setVisibility(View.GONE);
            statusSPBJ.setText(item.statusAppPJB);
            if (item.statusAppPJB.equals("R")) {
                statusSPBJ.setBackgroundResource(R.drawable.ring_background_reject);
                statusSPBJ.setVisibility(View.VISIBLE);
            } else if (item.statusAppPJB.equals("A")) {
                statusSPBJ.setVisibility(View.VISIBLE);
                statusSPBJ.setBackgroundResource(R.drawable.ring_background_approved);
            }  else if (item.statusAppPJB.equals("P")) {
                statusSPBJ.setVisibility(View.VISIBLE);
                statusSPBJ.setBackgroundResource(R.drawable.ring_background_pending);
            } else {
//                statusSPBJ.setVisibility(View.GONE);
            }
        }

        return v;
    }
}
