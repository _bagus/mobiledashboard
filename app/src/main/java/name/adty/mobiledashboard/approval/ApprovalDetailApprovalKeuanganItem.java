package name.adty.mobiledashboard.approval;

/**
 * Created by adty on 24/01/17.
 */

public class ApprovalDetailApprovalKeuanganItem {
    public final String tanggal;
    public final String oleh;
    public final String status;
    public final String note;

    ApprovalDetailApprovalKeuanganItem(String tanggal, String oleh, String status, String note) {
        this.tanggal= tanggal;
        this.oleh = oleh;
        this.status = status;
        this.note = note;
    }
}
