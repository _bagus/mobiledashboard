package name.adty.mobiledashboard.approval;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApprovalForum extends AppCompatActivity {

    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    SharedPreferences.Editor spe;
    String spbjid;
    ApprovalForumAdapter adapter;
    List<ApprovalForumItem> items = new ArrayList<ApprovalForumItem>();
    ListView listForum;
    EditText message;
    ImageButton reply;
    TextView badgeNotificationApproval;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_forum);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mobileDashboard = new MobileDashboard(ApprovalForum.this);
        sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        spe = sp.edit();
        spbjid = getIntent().getStringExtra("SPBJID");
        getSupportActionBar().setTitle(spbjid);

        listForum = (ListView) findViewById(R.id.listForum);
        adapter = new ApprovalForumAdapter(ApprovalForum.this, items);
        listForum.setAdapter(adapter);

        message = (EditText) findViewById(R.id.editPesan);
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listForum.setSelection(adapter.getCount() - 1);
            }
        });
        reply = (ImageButton) findViewById(R.id.buttonReply);
        reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!message.getText().toString().isEmpty()) {

                    String messageToSend = message.getText().toString();
                    message.setText("");

                    new OkHttpClient().newCall(new Request.Builder()
                            .url(mobileDashboard.APIURL + "/InsertForum" +
                                    "?IMEI=" + mobileDashboard.getIMEI() +
                                    "&Token=" + mobileDashboard.getToken() +
                                    "&SPBJID=" + spbjid +
                                    "&Komentar=" + messageToSend +
                                    "&UserID=" + sp.getString("MDUserID", ""))
                            .method("POST", RequestBody.create(null, new byte[0]))
                            .build()
                    ).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, final IOException e) {
                            ApprovalForum.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ApprovalForum.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            final String responseBody = response.body().string();
                            final JSONObject data;
                            try {
                                data = new JSONObject(responseBody);
                                if (data.getString("Value").equals("1")) {
                                    ApprovalForum.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            items.clear();
                                            message.setText("");
                                        }
                                    });
                                    loadMessage();
                                } else {
                                    ApprovalForum.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Toast.makeText(ApprovalForum.this, data.getString("Value"), Toast.LENGTH_LONG).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Toast.makeText(ApprovalForum.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                ApprovalForum.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Toast.makeText(ApprovalForum.this, new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                                        } catch (JSONException e1) {
                                            Toast.makeText(ApprovalForum.this, e1.getMessage().toString(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });

        updateNotifForum();

        loadMessage();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel("MD_FORUM", mobileDashboard.getNotifID(spbjid));

        Fabric.with(this, new Crashlytics());
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Approval Forum"));
    }

    public void updateNotifForum() {
        new OkHttpClient().newCall(
                new Request.Builder().url(mobileDashboard.APIURL + "/UpdateStatusNotifForum" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + spbjid +
                        "&UserID=" + sp.getString("MDUserID", ""))
                        .method("POST", RequestBody.create(null, new byte[0])).build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ApprovalForum.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ApprovalForum.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
            }
        });
    }

    public void loadMessage() {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetDataForum" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + spbjid +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ApprovalForum.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ApprovalForum.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                JSONArray data;
                try {
                    data = new JSONArray(responseBody);
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject dataObject = data.getJSONObject(i);
                        items.add(new ApprovalForumItem(
                                dataObject.getString("User"),
                                dataObject.getString("Komentar"),
                                dataObject.getString("Time")
                        ));
                    }
                } catch (JSONException e) {
                    ApprovalForum.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(ApprovalForum.this, new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(ApprovalForum.this, e1.getMessage().toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } finally {
                    ApprovalForum.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            listForum.setSelection(adapter.getCount() - 1);
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.approval_forum, menu);

        MenuItem item = menu.findItem(R.id.notification);
        MenuItemCompat.setActionView(item, R.layout.approval_forum_notification_badge);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        badgeNotificationApproval = (TextView) layout.findViewById(R.id.actionbar_notifcation_textview);
        ImageView image = (ImageView) layout.findViewById(R.id.image);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalForum.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        badgeNotificationApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalForum.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalForum.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        getCountNotifApproval();

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        spbjid = intent.getStringExtra("SPBJID");
        getSupportActionBar().setTitle(spbjid);
        loadMessage();
    }

    public void getCountNotifApproval() {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetCountNotifApproval" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + sp.getString("ApprovalDetailSPBJID", "") +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String notificationCountApproval = response.body().string();
                ApprovalForum.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (badgeNotificationApproval != null) {
                            badgeNotificationApproval.setText(notificationCountApproval);
                            if (!notificationCountApproval.equals("0")) {
                                badgeNotificationApproval.setVisibility(View.VISIBLE);
                            } else {
                                badgeNotificationApproval.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        getCountNotifApproval();
        loadMessage();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
}
}
