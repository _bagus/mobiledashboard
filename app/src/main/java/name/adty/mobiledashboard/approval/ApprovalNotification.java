package name.adty.mobiledashboard.approval;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApprovalNotification extends AppCompatActivity {

    List<ApprovalNotificationItem> items = new ArrayList<ApprovalNotificationItem>();
    ApprovalNotificationAdapter adapter;
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    SharedPreferences.Editor spe;
    TextView badgeNotificationApproval;

    ProgressBar progressBar;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobileDashboard = new MobileDashboard(ApprovalNotification.this);
        sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        spe = sp.edit();

        list = (ListView) findViewById(R.id.list);
        adapter = new ApprovalNotificationAdapter(ApprovalNotification.this, items);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ApprovalNotificationItem item = items.get(position);
                new OkHttpClient().newCall(new Request.Builder()
                        .url(mobileDashboard.APIURL + "/UpdateStatusNotif" +
                                "?IMEI=" + mobileDashboard.getIMEI() +
                                "&Token=" + mobileDashboard.getToken() +
                                "&NotifID=" + item.idPesan +
                                "&UserID=" + sp.getString("MDUserID", ""))
                        .method("POST", RequestBody.create(null, new byte[0]))
                        .build()
                ).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, final IOException e) {
                        ApprovalNotification.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ApprovalNotification.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String responseBody = response.body().string();
                        JSONObject data = null;
                        try {
                            data = new JSONObject(responseBody);
                            if (data.getString("Value").equals("1")) {
                                if (item.approval) {
                                    spe.putString("ApprovalDetailSPBJID", item.spbjId);
                                    if (item.approve.equals("true")) {
                                        Intent intent = new Intent(ApprovalNotification.this, ApprovalDetailTab.class);
                                        intent.putExtra("SPBJID", item.spbjId);
                                        spe.putString("ApprovalView", "approval");
                                        spe.putString("ApprovalDetailType", "approval");
                                        spe.commit();
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(ApprovalNotification.this, ApprovalDetailTab.class);
                                        intent.putExtra("SPBJID", item.spbjId);
                                        spe.putString("ApprovalView", "spbj");
                                        spe.putString("ApprovalDetailType", "spbj");
                                        spe.commit();
                                        startActivity(intent);
                                    }
                                } else {
                                    spe.putString("ApprovalView", "spbj");
                                    spe.putString("ApprovalDetailType", "spbj");
                                    spe.putString("ApprovalDetailSPBJID", item.spbjId);
                                    spe.commit();

                                    Intent intent = new Intent(ApprovalNotification.this, ApprovalForum.class);
                                    intent.putExtra("SPBJID", item.spbjId);
                                    startActivity(intent);
                                }
                            } else {
                                final JSONObject finalData = data;
                                ApprovalNotification.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Toast.makeText(ApprovalNotification.this, finalData.getString("Value"), Toast.LENGTH_LONG).show();
                                        } catch (JSONException e) {
                                            Toast.makeText(ApprovalNotification.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        } catch (final JSONException e) {
                            ApprovalNotification.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ApprovalNotification.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        getNotification();

        Fabric.with(this, new Crashlytics());
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Approval Notification"));
    }

    public void getNotification() {
        progressBar.setVisibility(View.VISIBLE);
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetNotification" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ApprovalNotification.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ApprovalNotification.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                JSONArray data = null;
                try {
                    data = new JSONArray(responseBody);
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        items.add(new ApprovalNotificationItem(
                                object.getInt("IDPesan"),
                                object.getBoolean("Approval"),
                                object.getString("Tgl"),
                                object.getString("Sender"),
                                object.getString("Ket"),
                                object.getString("SPBJID"),
                                object.getInt("Baca"),
                                object.getString("Approve")
                        ));
                    }
                } catch (JSONException e) {
                    ApprovalNotification.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            JSONObject dataObject = null;
                            try {
                                dataObject = new JSONObject(responseBody);
                                Toast.makeText(ApprovalNotification.this, dataObject.getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(ApprovalNotification.this, e1.getMessage(), Toast.LENGTH_LONG).show();
                                e1.printStackTrace();
                            }
                        }
                    });

                    e.printStackTrace();
                } finally {
                    ApprovalNotification.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void getCountNotifApproval() {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetCountNotifApproval" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + sp.getString("ApprovalDetailSPBJID", "") +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String notificationCountApproval = response.body().string();
                ApprovalNotification.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (badgeNotificationApproval != null) {
                            badgeNotificationApproval.setText(notificationCountApproval);
                            if (!notificationCountApproval.equals("0")) {
                                badgeNotificationApproval.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.approval_notification, menu);

        MenuItem item = menu.findItem(R.id.notification);
        MenuItemCompat.setActionView(item, R.layout.notification_badge);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        badgeNotificationApproval = (TextView) layout.findViewById(R.id.actionbar_notifcation_textview);
        ImageView image = (ImageView) layout.findViewById(R.id.image);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalNotification.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        badgeNotificationApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalNotification.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalNotification.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        getCountNotifApproval();

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
