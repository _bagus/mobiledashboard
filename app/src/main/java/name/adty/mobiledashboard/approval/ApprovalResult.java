package name.adty.mobiledashboard.approval;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ApprovalResult extends AppCompatActivity {

    TextView textResult;
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    SharedPreferences.Editor spe;
    TextView badgeNotificationApproval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobileDashboard = new MobileDashboard(ApprovalResult.this);
        sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        spe = sp.edit();

        textResult = (TextView) findViewById(R.id.textResult);
        textResult.setText(getIntent().getStringExtra("resultMessage"));

        Fabric.with(this, new Crashlytics());
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Approval Result"));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.approval_notification, menu);

        MenuItem item = menu.findItem(R.id.notification);
        MenuItemCompat.setActionView(item, R.layout.notification_badge);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        badgeNotificationApproval = (TextView) layout.findViewById(R.id.actionbar_notifcation_textview);
        ImageView image = (ImageView) layout.findViewById(R.id.image);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalResult.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        badgeNotificationApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalResult.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalResult.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        getCountNotifApproval();

        return true;
    }

    public void getCountNotifApproval() {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetCountNotifApproval" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + sp.getString("ApprovalDetailSPBJID", "") +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String notificationCountApproval = response.body().string();
                ApprovalResult.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (badgeNotificationApproval != null) {
                            badgeNotificationApproval.setText(notificationCountApproval);
                            if (!notificationCountApproval.equals("0")) {
                                badgeNotificationApproval.setVisibility(View.VISIBLE);
                            } else {
                                badgeNotificationApproval.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        getCountNotifApproval();
    }
}
