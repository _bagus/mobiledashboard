package name.adty.mobiledashboard.approval;

/**
 * Created by adty on 24/01/17.
 */

public class ApprovalItem {
    public final String name;
    public final String oleh;
    public final String tanggal1;
    public final String judul;
    public final int drawableId;
    public final String status;
    public final String statusAppPJB;

    ApprovalItem(String name, String judul, String oleh, String tanggal1, int drawableId, String status, String statusAppPJB) {
        this.name = name;
        this.drawableId = drawableId;
        this.judul = judul;
        this.tanggal1 = tanggal1;
        this.oleh = oleh;
        this.status= status;
        this.statusAppPJB = statusAppPJB;
    }
}
