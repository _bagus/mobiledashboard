package name.adty.mobiledashboard.approval;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.global.MDSQLite;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApprovalDetailTab extends AppCompatActivity {

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    SharedPreferences sp;
    SharedPreferences.Editor spe;
    MobileDashboard mobileDashboard;
    TextView badgeNotificationApproval;
    TextView badgeNotificationForum;
    String notificationCountApproval = "0";
    String notificationCountForum = "0";
    TabLayout tabLayout;
    NotificationManager notificationManager;

    public void updateStatusNotification(String IDPesan) {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/UpdateStatusNotif" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&NotifID=" + IDPesan +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .method("POST", RequestBody.create(null, new byte[0]))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_detail_tab);
        sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        spe = sp.edit();
        mobileDashboard = new MobileDashboard(ApprovalDetailTab.this);

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (getIntent().getBooleanExtra("NotificationClick", false)) {
            updateStatusNotification(getIntent().getStringExtra("NotificationIDPesan"));
            spe.remove("NotificationTag");
            spe.remove("NotificationID");
            spe.commit();
        } else {
            if (sp.getString("NotificationTag", "").equals("MD_APPROVAL")) {
                updateStatusNotification(sp.getString("NotificationIDPesan", ""));
                notificationManager.cancel("MD_APPROVAL", sp.getInt("NotificationID", 0));
                spe.remove("NotificationTag");
                spe.remove("NotificationID");
                spe.commit();
            } else if (sp.getString("NotificationTag", "").equals("MD_SPBJ")) {
                updateStatusNotification(sp.getString("NotificationIDPesan", ""));
                notificationManager.cancel("MD_FORUM", sp.getInt("NotificationID", 0));
                spe.remove("NotificationTag");
                spe.remove("NotificationID");
                spe.commit();
            }
        }

        TextView nomorSPBJ = (TextView) findViewById(R.id.nomor);
        nomorSPBJ.setText(getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE).getString("ApprovalDetailSPBJID", ""));
        TextView statusSPBJ = (TextView) findViewById(R.id.status);
        if (getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE).getString("ApprovalDetailStatus", "Normal").equals("Normal")) {
            statusSPBJ.setBackgroundResource(R.drawable.spbj_status_normal);
        } else {
            statusSPBJ.setBackgroundResource(R.drawable.spbj_status_urgent);
        }
        statusSPBJ.setText(getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE).getString("ApprovalDetailStatus", ""));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        if (sp.getString("ApprovalDetailType", "spbj").equals("approval")) {
            getSupportActionBar().setTitle("Approval");
        }

        Fabric.with(this, new Crashlytics());
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Approval Detail Tab"));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_approval_detail_tab, menu);

        MenuItem item = menu.findItem(R.id.notification);
        MenuItemCompat.setActionView(item, R.layout.notification_badge);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        badgeNotificationApproval = (TextView) layout.findViewById(R.id.actionbar_notifcation_textview);
        ImageView image = (ImageView) layout.findViewById(R.id.image);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        badgeNotificationApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        badgeNotificationApproval.setText(notificationCountApproval);

        MenuItem item2 = menu.findItem(R.id.forum);
        MenuItemCompat.setActionView(item2, R.layout.notification_badge_forum);
        RelativeLayout layout2 = (RelativeLayout) MenuItemCompat.getActionView(item2);
        badgeNotificationForum = (TextView) layout2.findViewById(R.id.actionbar_notifcation_textview);
        ImageView image2 = (ImageView) layout2.findViewById(R.id.image);

        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalDetailTab.this, ApprovalForum.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        badgeNotificationForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalDetailTab.this, ApprovalForum.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalDetailTab.this, ApprovalForum.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        badgeNotificationForum.setText(notificationCountForum);

        getCountNotifApproval();
        getCountNotifForum();

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        private MobileDashboard mobileDashboard;
        SharedPreferences sp;
        MDSQLite dbHelper;
        SQLiteDatabase dbRead;
        SQLiteDatabase dbWrite;
        String spbjid = "";
        List<String> budgetData = new ArrayList<String>();
        List<String> budgetDataID = new ArrayList<String>();
        List<ApprovalDataDetailItem> itemDetail = new ArrayList<ApprovalDataDetailItem>();
        ApprovalDataDetailAdapter detailAdapter;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = null;
            mobileDashboard = new MobileDashboard(getContext());
            sp = getContext().getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
            spbjid = sp.getString("ApprovalDetailSPBJID", "");
            dbHelper = new MDSQLite(getContext());
            dbRead = dbHelper.getReadableDatabase();
            dbWrite = dbHelper.getWritableDatabase();

            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1:
                    rootView = dataFile(inflater, container);
                    break;
                case 2:
                    rootView = dataDetail(inflater, container);
                    break;
                case 3:
                    rootView = dataApproval(inflater, container);
                    break;
            }
            return rootView;
        }

        public View dataApproval(LayoutInflater inflater, ViewGroup container) {
            View view = inflater.inflate(R.layout.content_approval_detail_3, container, false);

            Button buttonAtasan = (Button) view.findViewById(R.id.buttonAtasan);
            buttonAtasan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), ApprovalDetailApprovalAtasan.class);
                    intent.putExtra("SPBJID", spbjid);
                    startActivity(intent);
                }
            });

            Button buttonKeuangan = (Button) view.findViewById(R.id.buttonKeuangan);
            buttonKeuangan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), ApprovalDetailApprovalKeuangan.class);
                    intent.putExtra("SPBJID", spbjid);
                    startActivity(intent);
                }
            });

            return view;
        }

        public View dataDetail(final LayoutInflater inflater, final ViewGroup container) {
            View view = inflater.inflate(R.layout.content_approval_detail_2, container, false);

            LinearLayout saveContainer = (LinearLayout) view.findViewById(R.id.saveContainer);
            final ListView listDetail = (ListView) view.findViewById(R.id.listDetailSPBJ);

            if (sp.getString("ApprovalDetailType", "spbj").equals("approval")) {
                if (sp.getString("MDUserJabatan", "Atasan").equals("Atasan")) {
                    saveContainer.setVisibility(View.GONE);
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) listDetail.getLayoutParams();
                    layoutParams.setMargins(0, 0, 0, 0);
                    listDetail.setLayoutParams(layoutParams);
                }
            } else {
                saveContainer.setVisibility(View.GONE);
                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) listDetail.getLayoutParams();
                layoutParams.setMargins(0, 0, 0, 0);
                listDetail.setLayoutParams(layoutParams);
            }

            itemDetail = new ArrayList<ApprovalDataDetailItem>();
            detailAdapter = new ApprovalDataDetailAdapter(getContext(), itemDetail);
            listDetail.setAdapter(detailAdapter);

            getDataDetail();

            Button buttonSave = (Button) view.findViewById(R.id.buttonSave);
            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder passwordDialog = new AlertDialog.Builder(getContext());
                    passwordDialog.setView(inflater.inflate(R.layout.password_dialog, null))
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                    final AlertDialog dialog = passwordDialog.create();
                    dialog.show();
                    Button dialogOK = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    final EditText editTextPassword = (EditText) dialog.findViewById(R.id.editTextPassword);
                    dialogOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new OkHttpClient().newCall(
                                    new Request.Builder()
                                            .url(mobileDashboard.APIURL + "/CekPassword" +
                                                    "?IMEI=" + mobileDashboard.getIMEI() +
                                                    "&Token=" + mobileDashboard.getToken() +
                                                    "&Password=" + mobileDashboard.MD5(editTextPassword.getText().toString()) +
                                                    "&UserID=" + sp.getString("MDUserID", ""))
                                            .method("POST", RequestBody.create(null, new byte[0]))
                                            .build()
                            ).enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, final IOException e) {
                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onResponse(Call call, Response response) throws IOException {
                                    String responseBody = response.body().string();
                                    final JSONObject data;
                                    try {
                                        data = new JSONObject(responseBody);
                                        if (data.getString("Value").equals("1")) {
                                            for (int i = 0; i < itemDetail.size(); i++) {
                                                if (!itemDetail.get(i).itemID.isEmpty()) {
                                                    final int finalI = i;
                                                    new OkHttpClient().newCall(new Request.Builder()
                                                            .url(mobileDashboard.APIURL + "/UpdateDetilSPBJ" +
                                                                    "?IMEI=" + mobileDashboard.getIMEI() +
                                                                    "&Token=" + mobileDashboard.getToken() +
                                                                    "&SPBJID=" + spbjid +
                                                                    "&UserID=" + sp.getString("MDUserID", "") +
                                                                    "&ItemID=" + itemDetail.get(i).itemID +
                                                                    "&Quantity=" + sp.getString("QuantityForItemSPBJ" + spbjid + itemDetail.get(i).itemID, itemDetail.get(i).itemID) +
                                                                    "&BudgetID=" + sp.getString("BudgetForItemSPBJ" + spbjid + itemDetail.get(i).itemID, itemDetail.get(i).budgetID))
                                                            .method("POST", RequestBody.create(null, new byte[0]))
                                                            .build()
                                                    ).enqueue(new Callback() {
                                                        @Override
                                                        public void onFailure(Call call, final IOException e) {
                                                            e.printStackTrace();
                                                            if (getActivity() != null) {
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Toast.makeText(getContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                                                    }
                                                                });
                                                            }
                                                        }

                                                        @Override
                                                        public void onResponse(Call call, Response response) throws IOException {
                                                            String responseBody = response.body().string();
                                                            final JSONObject dataObject;
                                                            try {
                                                                dataObject = new JSONObject(responseBody);
                                                                if (dataObject.getString("Value").equals("1")) {
                                                                    if (getActivity() != null) {
                                                                        getActivity().runOnUiThread(new Runnable() {
                                                                            @Override
                                                                            public void run() {
                                                                                Toast.makeText(getContext(), itemDetail.get(finalI).name + " updated", Toast.LENGTH_LONG).show();
                                                                            }
                                                                        });
                                                                    }
                                                                } else {
                                                                    if (getActivity() != null) {
                                                                        getActivity().runOnUiThread(new Runnable() {
                                                                            @Override
                                                                            public void run() {
                                                                                try {
                                                                                    Toast.makeText(getContext(), dataObject.getString("Value"), Toast.LENGTH_LONG).show();
                                                                                } catch (JSONException e) {
                                                                                    e.printStackTrace();
                                                                                    Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                                Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                        } else {
                                            if (getActivity() != null) {
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            Toast.makeText(getContext(), data.getString("Value"), Toast.LENGTH_LONG).show();
                                                        } catch (JSONException e) {
                                                            Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    } catch (JSONException e) {
                                        Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                    } finally {
                                        if (getActivity() != null) {
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    getDataDetail();
                                                    dialog.dismiss();
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            });

            return view;
        }

        private void getDataDetail() {
            new OkHttpClient().newCall(new Request.Builder()
                    .url(mobileDashboard.APIURL + "/GetBudget" +
                            "?IMEI=" + mobileDashboard.getIMEI() +
                            "&Token=" + mobileDashboard.getToken() +
                            "&SPBJID=" + spbjid +
                            "&UserID=" + sp.getString("MDUserID", ""))
                    .build()
            ).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    final String responseBody = response.body().string();
                    JSONArray dataArray = null;
                    try {
                        dataArray = new JSONArray(responseBody);
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            budgetData.add(dataObject.getString("Nama"));
                            budgetDataID.add(dataObject.getString("BudgetID"));
                        }

                        new OkHttpClient().newCall(new Request.Builder()
                                .url(mobileDashboard.APIURL + "/GetDetilSPBJ" +
                                        "?IMEI=" + mobileDashboard.getIMEI() +
                                        "&Token=" + mobileDashboard.getToken() +
                                        "&SPBJID=" + spbjid +
                                        "&UserID=" + sp.getString("MDUserID", "")
                                )
                                .build()
                        ).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, final IOException e) {
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException {
                                final String responseBody = response.body().string();
                                JSONArray data = null;
                                itemDetail.clear();
                                try {
                                    data = new JSONArray(responseBody);
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject dataObject = data.getJSONObject(i);
                                        itemDetail.add(new ApprovalDataDetailItem(
                                                dataObject.getString("SPBJID"),
                                                dataObject.getString("Item"),
                                                dataObject.getString("Ket"),
                                                dataObject.getInt("Quantity"),
                                                dataObject.getLong("HargaSatuan"),
                                                dataObject.getLong("Total"),
                                                dataObject.getString("ItemID"),
                                                dataObject.getString("Satuan"),
                                                dataObject.getString("BudgetID"),
                                                dataObject.getString("Budget"),
                                                budgetData,
                                                budgetDataID
                                        ));
                                    }

                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                detailAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                } catch (JSONException e) {
                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    Toast.makeText(getContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                                                } catch (JSONException e1) {
                                                    Toast.makeText(getContext(), e1.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                                    }
                                } finally {

                                }
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Toast.makeText(getContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getContext(), e1.getMessage().toString(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }

        public View dataFile(LayoutInflater inflater, ViewGroup container) {
            View rootView = inflater.inflate(R.layout.content_approval_detail, container, false);

            final TextView textTanggal = (TextView) rootView.findViewById(R.id.textTanggal);
            final TextView textProject = (TextView) rootView.findViewById(R.id.textProject);
            final TextView textPemohon = (TextView) rootView.findViewById(R.id.textPemohon);
            final TextView textStatus = (TextView) rootView.findViewById(R.id.textStatus);
            final TextView textAlasan = (TextView) rootView.findViewById(R.id.textAlasan);
            final TextView textTarget = (TextView) rootView.findViewById(R.id.textTargetWaktu);
            final TextView textTotal = (TextView) rootView.findViewById(R.id.textTotal);
            final RelativeLayout approvalContainer = (RelativeLayout) rootView.findViewById(R.id.approvalContainer);

            final EditText editNote = (EditText) rootView.findViewById(R.id.editNote);

            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel("MD_FORUM", mobileDashboard.getNotifID(spbjid));

            if (sp.getString("ApprovalDetailType", "spbj").equals("approval")) {
                approvalContainer.setVisibility(View.VISIBLE);
                notificationManager.cancel("MD_APPROVAL", mobileDashboard.getNotifID(spbjid));
            } else {
                approvalContainer.setVisibility(View.GONE);
                notificationManager.cancel("MD_SPBJ", mobileDashboard.getNotifID(spbjid));
            }

            AlertDialog.Builder passwordDialog = new AlertDialog.Builder(getContext());
            passwordDialog.setView(inflater.inflate(R.layout.password_dialog, null))
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            final AlertDialog dialog = passwordDialog.create();

            Button btnApproval = (Button) rootView.findViewById(R.id.btnApprove);
            btnApproval.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.show();
                    Button dialogOK = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    final EditText editTextPassword = (EditText) dialog.findViewById(R.id.editTextPassword);
                    dialogOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new OkHttpClient().newCall(
                                    new Request.Builder()
                                            .url(mobileDashboard.APIURL + "/CekPassword" +
                                                    "?IMEI=" + mobileDashboard.getIMEI() +
                                                    "&Token=" + mobileDashboard.getToken() +
                                                    "&Password=" + mobileDashboard.MD5(editTextPassword.getText().toString()) +
                                                    "&UserID=" + sp.getString("MDUserID", ""))
                                            .method("POST", RequestBody.create(null, new byte[0]))
                                            .build()
                            ).enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, final IOException e) {
                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onResponse(Call call, Response response) throws IOException {
                                    String responseBody = response.body().string();
                                    final JSONObject data;
                                    try {
                                        data = new JSONObject(responseBody);
                                        if (data.getString("Value").equals("1")) {
                                            new OkHttpClient().newCall(new Request.Builder()
                                                    .url(mobileDashboard.APIURL + "/InsertApproval" +
                                                            "?IMEI=" + mobileDashboard.getIMEI() +
                                                            "&Token=" + mobileDashboard.getToken() +
                                                            "&SPBJID=" + spbjid +
                                                            "&Note=" + editNote.getText().toString() +
                                                            "&Response=TRUE" +
                                                            "&UserID=" + sp.getString("MDUserID", ""))
                                                    .method("POST", RequestBody.create(null, new byte[0]))
                                                    .build()
                                            ).enqueue(new Callback() {
                                                @Override
                                                public void onFailure(Call call, final IOException e) {
                                                    if (getActivity() != null) {
                                                        getActivity().runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(getContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                                            }
                                                        });
                                                    }
                                                }

                                                @Override
                                                public void onResponse(Call call, Response response) throws IOException {
                                                    final String responseBody = response.body().string();
                                                    final JSONObject data;
                                                    try {
                                                        data = new JSONObject(responseBody);
                                                        if (data.getString("Value").equals("1")) {
                                                            if (getActivity() != null) {
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        approvalContainer.setVisibility(View.GONE);
                                                                        editNote.setText("");
//                                                                        Toast.makeText(getContext(), "Approval Success", Toast.LENGTH_LONG).show();
                                                                        ContentValues val = new ContentValues();
                                                                        val.put("spbjid", spbjid);
                                                                        long newRowid = dbWrite.insert("approved_spbj", null, val);
                                                                        dialog.dismiss();
                                                                    }
                                                                });

                                                                Intent intent = new Intent(getActivity(), ApprovalResult.class);
                                                                intent.putExtra("resultMessage", "Anda telah berhasil approve SPBJ No : " + spbjid);
                                                                getActivity().startActivity(intent);
                                                                getActivity().finish();

                                                            }
                                                        } else {
                                                            if (getActivity() != null) {
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        try {
                                                                            Toast.makeText(getContext(), data.getString("Value"), Toast.LENGTH_LONG).show();
                                                                        } catch (JSONException e) {
                                                                            Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    } catch (JSONException e) {
                                                        if (getActivity() != null) {
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        Toast.makeText(getContext(), new JSONObject(responseBody).getString("Message"), Toast.LENGTH_LONG).show();
                                                                    } catch (JSONException e1) {
                                                                        Toast.makeText(getContext(), e1.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            });
                                        } else {
                                            if (getActivity() != null) {
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            Toast.makeText(getContext(), data.getString("Value"), Toast.LENGTH_LONG).show();
                                                        } catch (JSONException e) {
                                                            Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    } catch (JSONException e) {
                                        Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                    });
                }
            });

            Button btnReject = (Button) rootView.findViewById(R.id.btnReject);
            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editNote.getText().toString().isEmpty()) {
                        Toast.makeText(getContext(), "Alasan wajib diisi!", Toast.LENGTH_LONG).show();
                    } else {
                        dialog.show();
                        Button dialogOK = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                        final EditText editTextPassword = (EditText) dialog.findViewById(R.id.editTextPassword);
                        dialogOK.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new OkHttpClient().newCall(
                                        new Request.Builder()
                                                .url(mobileDashboard.APIURL + "/CekPassword" +
                                                        "?IMEI=" + mobileDashboard.getIMEI() +
                                                        "&Token=" + mobileDashboard.getToken() +
                                                        "&Password=" + mobileDashboard.MD5(editTextPassword.getText().toString()) +
                                                        "&UserID=" + sp.getString("MDUserID", ""))
                                                .method("POST", RequestBody.create(null, new byte[0]))
                                                .build()
                                ).enqueue(new Callback() {
                                    @Override
                                    public void onFailure(Call call, IOException e) {
                                        if (getActivity() != null) {
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onResponse(Call call, Response response) throws IOException {
                                        String responseBody = response.body().string();
                                        final JSONObject data;
                                        try {
                                            data = new JSONObject(responseBody);
                                            if (data.getString("Value").equals("1")) {
                                                new OkHttpClient().newCall(new Request.Builder()
                                                        .url(mobileDashboard.APIURL + "/InsertApproval" +
                                                                "?IMEI=" + mobileDashboard.getIMEI() +
                                                                "&Token=" + mobileDashboard.getToken() +
                                                                "&SPBJID=" + spbjid +
                                                                "&Note=" + editNote.getText().toString() +
                                                                "&Response=FALSE" +
                                                                "&UserID=" + sp.getString("MDUserID", ""))
                                                        .method("POST", RequestBody.create(null, new byte[0]))
                                                        .build()
                                                ).enqueue(new Callback() {
                                                    @Override
                                                    public void onFailure(Call call, final IOException e) {
                                                        if (getActivity() != null) {
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                                                }
                                                            });
                                                        }
                                                    }

                                                    @Override
                                                    public void onResponse(Call call, Response response) throws IOException {
                                                        final String responseBody = response.body().string();
                                                        final JSONObject data;
                                                        try {
                                                            data = new JSONObject(responseBody);
                                                            if (data.getString("Value").equals("1")) {
                                                                if (getActivity() != null) {
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            approvalContainer.setVisibility(View.GONE);
                                                                            editNote.setText("");
                                                                            ContentValues val = new ContentValues();
                                                                            val.put("spbjid", spbjid);
                                                                            dialog.dismiss();
                                                                        }
                                                                    });

                                                                    Intent intent = new Intent(getActivity(), ApprovalResult.class);
                                                                    intent.putExtra("resultMessage", "Anda telah melakukan reject SPBJ No : " + spbjid);
                                                                    getActivity().startActivity(intent);
                                                                    getActivity().finish();

                                                                }
                                                            } else {
                                                                if (getActivity() != null) {
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            try {
                                                                                Toast.makeText(getContext(), data.getString("Value"), Toast.LENGTH_LONG).show();
                                                                            } catch (JSONException e) {
                                                                                Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        } catch (JSONException e) {
                                                            if (getActivity() != null) {
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        try {
                                                                            Toast.makeText(getContext(), new JSONObject(responseBody).getString("Message"), Toast.LENGTH_LONG).show();
                                                                        } catch (JSONException e1) {
                                                                            Toast.makeText(getContext(), e1.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                });
                                            } else {
                                                if (getActivity() != null) {
                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            try {
                                                                Toast.makeText(getContext(), data.getString("Value"), Toast.LENGTH_LONG).show();
                                                            } catch (JSONException e) {
                                                                Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        } catch (JSONException e) {
                                            Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });

            String apiFun = "GetDataFileSPBJ";
            if (sp.getString("ApprovalDetailType", "spbj").equals("approval")) {
                apiFun = "GetDataFileApproval";
            }

            new OkHttpClient().newCall(new Request.Builder()
                    .url(mobileDashboard.APIURL + "/" + apiFun +
                            "?IMEI=" + mobileDashboard.getIMEI() +
                            "&Token=" + mobileDashboard.getToken() +
                            "&SPBJID=" + sp.getString("ApprovalDetailSPBJID", "") +
                            "&UserID=" + sp.getString("MDUserID", ""))
                    .build()
            ).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    final String responseBody = response.body().string();
                    JSONArray data = null;
                    try {
                        data = new JSONArray(responseBody);
                        final JSONObject dataObject = new JSONObject(data.getString(0));

                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        TextView globalStatus = (TextView) getActivity().findViewById(R.id.status);
                                        if (dataObject.getString("StatusTarget").equals("Normal")) {
                                            globalStatus.setBackgroundResource(R.drawable.spbj_status_normal);
                                        } else {
                                            globalStatus.setBackgroundResource(R.drawable.spbj_status_urgent);
                                        }
                                        globalStatus.setText(dataObject.getString("StatusTarget"));

                                        textTanggal.setText(dataObject.getString("TglSPBJ"));
                                        textProject.setText(dataObject.getString("Project"));
                                        textPemohon.setText(dataObject.getString("Pemohon"));
                                        textStatus.setText(dataObject.getString("Status"));
                                        textAlasan.setText(dataObject.getString("AlasanUrgent"));
                                        textTarget.setText(dataObject.getString("TargetHari") + " Hari");
                                        textTotal.setText(NumberFormat.getInstance(Locale.US)
                                                .format(Double.parseDouble(dataObject.getString("Total"))).replace(",", "."));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }

                    } catch (JSONException e) {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Toast.makeText(getContext(), new JSONObject(responseBody).getString("Message"), Toast.LENGTH_LONG).show();
                                    } catch (JSONException e1) {
                                        Toast.makeText(getContext(), "API response error", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                    }
                }
            });

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            SharedPreferences sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
            if (sp.getString("ApprovalDetailUserType", "").equals("0")) {
                return 2;
            } else {
                return 3;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "DATA FILE";
                case 1:
                    return "DETAIL";
                case 2:
                    return "STATUS APPROVAL";
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getCountNotifApproval();
        getCountNotifForum();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        getCountNotifApproval();
        getCountNotifForum();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
    }

    public void getCountNotifForum() {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetCountNotifForum" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + sp.getString("ApprovalDetailSPBJID", "") +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                notificationCountForum = response.body().string();
                ApprovalDetailTab.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (badgeNotificationForum != null) {
                            badgeNotificationForum.setText(notificationCountForum);
                            if (!notificationCountForum.equals("0")) {
                                badgeNotificationForum.setVisibility(View.VISIBLE);
                            } else {
                                badgeNotificationForum.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        });
    }

    public void getCountNotifApproval() {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetCountNotifApproval" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + sp.getString("ApprovalDetailSPBJID", "") +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String notificationCountApproval = response.body().string();
                ApprovalDetailTab.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (badgeNotificationApproval != null) {
                            badgeNotificationApproval.setText(notificationCountApproval);
                            if (!notificationCountApproval.equals("0")) {
                                badgeNotificationApproval.setVisibility(View.VISIBLE);
                            } else {
                                badgeNotificationApproval.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        });
    }
}
