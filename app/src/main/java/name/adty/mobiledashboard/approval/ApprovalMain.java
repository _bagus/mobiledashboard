package name.adty.mobiledashboard.approval;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.Login;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.approval.filter.FilterSPBJ;
import name.adty.mobiledashboard.global.MobileDashboard;
import name.adty.mobiledashboard.report.ReportMain;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApprovalMain extends AppCompatActivity {

    private static final String TAG = "ApprovalMain";

    private SlidingUpPanelLayout mLayout;
    TextView textUserName;
    TextView textUserRole;
    LinearLayout buttonReport;
    LinearLayout buttonApproval;
    Toolbar mainToolbar;
    MenuInflater menuInflater;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spe;
    MobileDashboard mobileDashboard;
    List<ApprovalItem> items = new ArrayList<ApprovalItem>();
    ProgressBar progressBar;
    ListView gridView;
    ApprovalAdapter approvalAdapter = null;
    TabLayout tabLayout;
    boolean isGetApproval = true;
    boolean isGetSPBJ = false;
    public final static int FILTER = 0;
    TextView badgeNotificationApproval;
    boolean isActivityResult = false;

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mobileDashboard = new MobileDashboard(ApprovalMain.this);
        sharedPreferences = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        spe = sharedPreferences.edit();

        mainToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mainToolbar.setTitle("Mobile Approval");
        mainToolbar.inflateMenu(R.menu.approval_main);
        mainToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.filter:
                        Intent intentFilterSPBJ = new Intent(ApprovalMain.this, FilterSPBJ.class);
                        startActivityForResult(intentFilterSPBJ, FILTER);
                        break;
                    case R.id.notification:
                        Intent intentNotification = new Intent(ApprovalMain.this, ApprovalNotification.class);
                        startActivityForResult(intentNotification, FILTER);
                        break;
                }

                return true;
            }
        });

        MenuItem menuNotification = mainToolbar.getMenu().findItem(R.id.notification);
        MenuItemCompat.setActionView(menuNotification, R.layout.notification_badge);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(menuNotification);
        badgeNotificationApproval = (TextView) layout.findViewById(R.id.actionbar_notifcation_textview);
        ImageView image = (ImageView) layout.findViewById(R.id.image);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalMain.this, ApprovalNotification.class);
                startActivity(intent);
            }
        });

        badgeNotificationApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalMain.this, ApprovalNotification.class);
                startActivity(intent);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalMain.this, ApprovalNotification.class);
                startActivity(intent);
            }
        });

        getCountNotifApproval();

        menuInflater = getMenuInflater();

        buttonApproval = (LinearLayout) findViewById(R.id.buttonApproval);
        buttonReport = (LinearLayout) findViewById(R.id.buttonReport);

        if (!sharedPreferences.getBoolean("MDUserMenuReport", false)) {
            buttonReport.setVisibility(View.GONE);
        }

        if (!sharedPreferences.getBoolean("MDUserMenuApproval", false)) {
            buttonApproval.setVisibility(View.GONE);
        }

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (previousState == SlidingUpPanelLayout.PanelState.ANCHORED && newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.VISIBLE);
                } else if (previousState == SlidingUpPanelLayout.PanelState.COLLAPSED && newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.GONE);
                } else if (newState != SlidingUpPanelLayout.PanelState.COLLAPSED && newState != SlidingUpPanelLayout.PanelState.HIDDEN && newState != SlidingUpPanelLayout.PanelState.DRAGGING) {
                    mainToolbar.setVisibility(View.GONE);
                } else {
                    mainToolbar.setVisibility(View.VISIBLE);
                }
            }
        });

        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
                } else if (mLayout.getPanelState() != SlidingUpPanelLayout.PanelState.HIDDEN) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            }
        });

        textUserName = (TextView) findViewById(R.id.textUserName);
        textUserName.setText(sharedPreferences.getString("MDNamaUser", ""));
        textUserRole = (TextView) findViewById(R.id.textUserRole);
        textUserRole.setText(sharedPreferences.getString("MDUserRole", ""));

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        gridView = (ListView) findViewById(R.id.approvalContainer);
        approvalAdapter = new ApprovalAdapter(ApprovalMain.this, items);
        gridView.setAdapter(approvalAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabContainer);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                SharedPreferences.Editor spe = sharedPreferences.edit();
                switch (tab.getPosition()) {
                    case 0:
                        isGetSPBJ = true;
                        isGetApproval = false;
                        spe.putString("ApprovalView", "spbj");
                        spe.commit();
                        getSPBJ();
                        break;
                    case 1:
                        isGetSPBJ = false;
                        isGetApproval = true;
                        spe.putString("ApprovalView", "approval");
                        spe.commit();
                        getApproval();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if (sharedPreferences.getString("ApprovalView", "approval").equals("approval")) {
            tabLayout.getTabAt(1).select();
        }


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ApprovalItem item = items.get(position);
                Intent intentDetailApproval = new Intent(ApprovalMain.this, ApprovalDetailTab.class);

                SharedPreferences.Editor spe = ApprovalMain.this.getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE).edit();
                spe.putString("ApprovalDetailSPBJID", item.name);
                spe.putString("ApprovalDetailStatus", item.status);
                spe.putString("ApprovalDetailUserType", item.statusAppPJB);

                if (isGetApproval) {
                    spe.putString("ApprovalDetailType", "approval");
                } else {
                    spe.putString("ApprovalDetailType", "spbj");
                }

                spe.commit();

                startActivity(intentDetailApproval);
            }
        });

        Fabric.with(this, new Crashlytics());
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Approval Main"));

    }

    public void clickButtonLogout(View view) {
        new AlertDialog.Builder(ApprovalMain.this)
                .setTitle("Logout Confirmation")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new OkHttpClient().newCall(new Request.Builder()
                                .url(mobileDashboard.APIURL + "/Logout" +
                                        "?IMEI=" + mobileDashboard.getIMEI() +
                                        "&Token=" + mobileDashboard.getToken() +
                                        "&FirebaseID=" + sharedPreferences.getString("firebaseToken", "") +
                                        "&UserID=" + sharedPreferences.getString("MDUserID", ""))
                                .method("POST", RequestBody.create(null, new byte[0]))
                                .build()
                        ).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, final IOException e) {
                                ApprovalMain.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(ApprovalMain.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                String responseBody = response.body().string();
                                JSONObject object = null;
                                try {
                                    object = new JSONObject(responseBody);
                                    if (object.getString("Value").equals("1")) {
                                        Answers.getInstance().logCustom(new CustomEvent("Logout"));

                                        SharedPreferences.Editor spe = sharedPreferences.edit();
                                        spe.putString("MobileDashboardLogin", "");
                                        spe.clear();
                                        spe.commit();
                                        startActivity(new Intent(ApprovalMain.this, Login.class));
                                        finish();
                                    }
                                } catch (final JSONException e) {
                                    ApprovalMain.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ApprovalMain.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("No", null).show();
    }

    public void clickButtonReport(View view) {
        Intent intent = new Intent(ApprovalMain.this, ReportMain.class);
        startActivity(intent);
        finish();
    }

    public void clickButtonApproval(View view) {
        spe.putString("ApprovalView", "approval");
        spe.commit();

        Intent intent = new Intent(ApprovalMain.this, ApprovalMain.class);
        startActivity(intent);
        finish();
    }

    public void clickUpDrawer(View view) {
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    public void getApproval() {
        progressBar.setVisibility(View.VISIBLE);
        new OkHttpClient().newCall(new Request.Builder()
                .url(MobileDashboard.APIURL + "/GetListApproval?" +
                        "IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&UserID=" + sharedPreferences.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ApprovalMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ApprovalMain.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                items.clear();
                final String responseString = response.body().string();
                JSONArray responseArray = null;
                try {
                    responseArray = new JSONArray(responseString);
                    for (int i = 0; i < responseArray.length(); i++) {
                        items.add(new ApprovalItem(
                                responseArray.getJSONObject(i).getString("SPBJID"),
                                responseArray.getJSONObject(i).getString("Ket"),
                                responseArray.getJSONObject(i).getString("PersonRequest"),
                                responseArray.getJSONObject(i).getString("TglSPBJ"),
                                R.mipmap.ic_spbj,
                                responseArray.getJSONObject(i).getString("Status"),
                                responseArray.getJSONObject(i).getString("StatusAppPjb")
                        ));
                    }
                } catch (final JSONException e) {
                    ApprovalMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(ApprovalMain.this, new JSONObject(responseString).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(ApprovalMain.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } finally {
                    ApprovalMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            approvalAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    public void getSPBJ() {
        progressBar.setVisibility(View.VISIBLE);
        new OkHttpClient().newCall(new Request.Builder()
                .url(MobileDashboard.APIURL + "/GetListSPBJ?" +
                        "IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&UserID=" + sharedPreferences.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ApprovalMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ApprovalMain.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                items.clear();
                final String responseString = response.body().string();
                JSONArray responseArray = null;
                try {
                    responseArray = new JSONArray(responseString);
                    for (int i = 0; i < responseArray.length(); i++) {
                        items.add(new ApprovalItem(
                                responseArray.getJSONObject(i).getString("SPBJID"),
                                responseArray.getJSONObject(i).getString("Ket"),
                                responseArray.getJSONObject(i).getString("PersonRequest"),
                                responseArray.getJSONObject(i).getString("TglSPBJ"),
                                R.mipmap.ic_spbj,
                                responseArray.getJSONObject(i).getString("Status"),
                                responseArray.getJSONObject(i).getString("StatusApp")
                        ));
                    }
                } catch (final JSONException e) {
                    ApprovalMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(ApprovalMain.this, new JSONObject(responseString).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(ApprovalMain.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } finally {
                    ApprovalMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            approvalAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    public void getSPBJFilter(String dari, String sampai, String statusSPBJ, String statusApproval, String project, String divisi, String tipe) {
        progressBar.setVisibility(View.VISIBLE);
        new OkHttpClient().newCall(new Request.Builder()
                .url(MobileDashboard.APIURL + "/GetSPBJ" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&Dari=" + dari +
                        "&Sampai=" + sampai +
                        "&Status=" + statusSPBJ +
                        "&StatusApp=" + statusApproval +
                        "&Project=" + project +
                        "&HRDept=" + divisi +
                        "&Tipe=" + tipe +
                        "&UserID=" + sharedPreferences.getString("MDUserID", "")
                )
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ApprovalMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ApprovalMain.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                items.clear();
                final String responseString = response.body().string();
                JSONArray responseArray = null;
                try {
                    responseArray = new JSONArray(responseString);
                    for (int i = 0; i < responseArray.length(); i++) {
                        items.add(new ApprovalItem(
                                responseArray.getJSONObject(i).getString("SPBJID"),
                                responseArray.getJSONObject(i).getString("Ket"),
                                responseArray.getJSONObject(i).getString("PersonRequest"),
                                responseArray.getJSONObject(i).getString("TglSPBJ"),
                                R.mipmap.ic_spbj,
                                responseArray.getJSONObject(i).getString("Status"),
                                responseArray.getJSONObject(i).getString("StatusApp")
                        ));
                    }
                } catch (final JSONException e) {
                    ApprovalMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(ApprovalMain.this, new JSONObject(responseString).getString("Message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(ApprovalMain.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } finally {
                    ApprovalMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            approvalAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    public void getApprovalFilter(String dari, String sampai, String statusSPBJ, String statusApproval, String project, String divisi, String tipe) {
        progressBar.setVisibility(View.VISIBLE);
        new OkHttpClient().newCall(new Request.Builder()
                .url(MobileDashboard.APIURL + "/GetApproval" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&Dari=" + dari +
                        "&Sampai=" + sampai +
                        "&Status=" + statusSPBJ +
                        "&StatusApp=" + statusApproval +
                        "&Project=" + project +
                        "&HRDept=" + divisi +
                        "&Tipe=" + tipe +
                        "&UserID=" + sharedPreferences.getString("MDUserID", "")
                )
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                ApprovalMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ApprovalMain.this, getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                items.clear();
                final String responseString = response.body().string();
                JSONArray responseArray = null;
                try {
                    responseArray = new JSONArray(responseString);
                    for (int i = 0; i < responseArray.length(); i++) {
                        items.add(new ApprovalItem(
                                responseArray.getJSONObject(i).getString("SPBJID"),
                                responseArray.getJSONObject(i).getString("Ket"),
                                responseArray.getJSONObject(i).getString("PersonRequest"),
                                responseArray.getJSONObject(i).getString("TglSPBJ"),
                                R.mipmap.ic_spbj,
                                responseArray.getJSONObject(i).getString("Status"),
                                responseArray.getJSONObject(i).getString("StatusAppPjb")
                        ));
                    }
                } catch (final JSONException e) {
                    ApprovalMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(ApprovalMain.this, new JSONObject(responseString).getString("Message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(ApprovalMain.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } finally {
                    ApprovalMain.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            approvalAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILTER) {
            if (resultCode == RESULT_OK) {
                if (sharedPreferences.getString("ApprovalView", "spbj").equals("spbj")) {
                    getSPBJFilter(
                            data.getStringExtra("dari"),
                            data.getStringExtra("sampai"),
                            data.getStringExtra("statusSPBJ"),
                            data.getStringExtra("statusApproval"),
                            data.getStringExtra("project"),
                            data.getStringExtra("divisi"),
                            data.getStringExtra("tipe")
                    );
                } else {
                    getApprovalFilter(
                            data.getStringExtra("dari"),
                            data.getStringExtra("sampai"),
                            data.getStringExtra("statusSPBJ"),
                            data.getStringExtra("statusApproval"),
                            data.getStringExtra("project"),
                            data.getStringExtra("divisi"),
                            data.getStringExtra("tipe")
                    );
                }
                isActivityResult = true;
            }
        }
    }

    public void getCountNotifApproval() {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetCountNotifApproval" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + sharedPreferences.getString("ApprovalDetailSPBJID", "") +
                        "&UserID=" + sharedPreferences.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String notificationCountApproval = response.body().string();
                ApprovalMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (badgeNotificationApproval != null) {
                            badgeNotificationApproval.setText(notificationCountApproval);
                            if (!notificationCountApproval.equals("0")) {
                                badgeNotificationApproval.setVisibility(View.VISIBLE);
                            } else {
                                badgeNotificationApproval.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        getCountNotifApproval();

        if (!isActivityResult) {
            if (sharedPreferences.getString("ApprovalView", "approval").equals("approval")) {
                getApproval();
            } else {
                getSPBJ();
            }
            isActivityResult = false;
        }
    }
}