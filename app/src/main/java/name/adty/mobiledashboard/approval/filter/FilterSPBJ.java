package name.adty.mobiledashboard.approval.filter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import name.adty.mobiledashboard.R;
import name.adty.mobiledashboard.approval.ApprovalMain;
import name.adty.mobiledashboard.approval.ApprovalNotification;
import name.adty.mobiledashboard.approval.ApprovalResult;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FilterSPBJ extends AppCompatActivity {

    String dariTanggal;
    String sampaiTanggal;
    String statusSPBJ;
    String statusApproval;
    String project;
    String divisi;
    String tipe;
    LinearLayout buttonDateDari;
    LinearLayout buttonDateSampai;
    TextView textDari;
    TextView textSampai;
    Spinner spinnerProject;
    Spinner spinnerStatusSPBJ;
    Spinner spinnerStatusApproval;
    Spinner spinnerDivisi;
    Spinner spinnerType;
    List<String> dataStatusSPBJ = new ArrayList<String>();
    List<String> dataStatusSPBJKet = new ArrayList<String>();
    List<String> dataStatusApproval = new ArrayList<String>();
    List<String> dataStatusApprovalKet = new ArrayList<String>();
    List<String> dataProject = new ArrayList<String>();
    List<String> dataProjectID = new ArrayList<String>();
    List<String> dataDivisi = new ArrayList<String>();
    List<String> dataDivisiID = new ArrayList<String>();
    List<String> dataTipe = new ArrayList<String>();
    List<String> dataTipeID = new ArrayList<String>();
    MobileDashboard mobileDashboard;
    SharedPreferences sp;
    TextView badgeNotificationApproval;

    static final String namaBulan[] = {"Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_spbj);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobileDashboard = new MobileDashboard(getApplicationContext());
        sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        buttonDateDari = (LinearLayout) findViewById(R.id.buttonDateDari);
        buttonDateSampai = (LinearLayout) findViewById(R.id.buttonDateSampai);
        textDari = (TextView) findViewById(R.id.textDari);
        textSampai = (TextView) findViewById(R.id.textSampai);
        spinnerStatusSPBJ = (Spinner) findViewById(R.id.spinnerStatusSPBJ);

        dariTanggal = String.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)) + "-"
                + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + "-"
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        sampaiTanggal = String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) + "-"
                + namaBulan[Calendar.getInstance().get(Calendar.MONTH)]
                + "-" + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        textDari.setText(String.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)) + " "
                + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
        textSampai.setText(String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) + " "
                + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));

        buttonDateDari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(FilterSPBJ.this, R.style.customDatePickerDialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                dariTanggal = String.valueOf(dayOfMonth)
                                        + "-" + namaBulan[monthOfYear] + "-"
                                        + String.valueOf(year);
                                textDari.setText(String.valueOf(dayOfMonth) + " "
                                        + namaBulan[monthOfYear] + " " + String.valueOf(year));
                            }
                        },
                        Calendar.getInstance().get(Calendar.YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)
                );

                dpd.show();
            }
        });

        buttonDateSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(FilterSPBJ.this, R.style.customDatePickerDialog,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                sampaiTanggal = String.valueOf(dayOfMonth)
                                        + "-" + namaBulan[monthOfYear] + "-"
                                        + String.valueOf(year);
                                textSampai.setText(String.valueOf(dayOfMonth) + " "
                                        + namaBulan[monthOfYear] + " " + String.valueOf(year));
                            }
                        },
                        Calendar.getInstance().get(Calendar.YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)
                );

                dpd.show();
            }
        });

        final ArrayAdapter<String> adapterSpinnerStatusSPBJ = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataStatusSPBJKet);
        adapterSpinnerStatusSPBJ.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatusSPBJ = (Spinner) findViewById(R.id.spinnerStatusSPBJ);
        spinnerStatusSPBJ.setAdapter(adapterSpinnerStatusSPBJ);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetStatusSPBJ" +
                        "?Token=" + mobileDashboard.getToken() +
                        "&IMEI=" + mobileDashboard.getIMEI() +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterSPBJ.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataStatusSPBJ.add(responseArray.getJSONObject(i).getString("Status"));
                        dataStatusSPBJKet.add(responseArray.getJSONObject(i).getString("Ket"));
                    }
                    statusSPBJ = responseArray.getJSONObject(0).getString("Status");
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterSpinnerStatusSPBJ.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Respose Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
        spinnerStatusSPBJ.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statusSPBJ = dataStatusSPBJ.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ArrayAdapter<String> adapterSpinnerStatusApproval = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataStatusApprovalKet);
        adapterSpinnerStatusSPBJ.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatusApproval = (Spinner) findViewById(R.id.spinnerStatusApproval);
        spinnerStatusApproval.setAdapter(adapterSpinnerStatusApproval);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetStatusApproval" +
                        "?Token=" + mobileDashboard.getToken() +
                        "&IMEI=" + mobileDashboard.getIMEI() +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterSPBJ.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataStatusApproval.add(responseArray.getJSONObject(i).getString("Status"));
                        dataStatusApprovalKet.add(responseArray.getJSONObject(i).getString("Ket"));
                    }
                    statusApproval = responseArray.getJSONObject(0).getString("Status");
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterSpinnerStatusApproval.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Respose Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
        spinnerStatusApproval.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statusApproval = dataStatusApproval.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataProject);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProject = (Spinner) findViewById(R.id.spinnerProject);
        spinnerProject.setAdapter(adapterProject);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetProject" +
                        "?Token=" + mobileDashboard.getToken() +
                        "&IMEI=" + mobileDashboard.getIMEI() +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterSPBJ.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataProject.add(responseArray.getJSONObject(i).getString("NamaProject"));
                        dataProjectID.add(responseArray.getJSONObject(i).getString("Project"));
                    }
                    project = responseArray.getJSONObject(0).getString("Project");
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterProject.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
        spinnerProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                project = dataProjectID.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ArrayAdapter<String> adapterDivisi = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataDivisi);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDivisi = (Spinner) findViewById(R.id.spinnerDivisi);
        spinnerDivisi.setAdapter(adapterDivisi);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetHRDept" +
                        "?Token=" + mobileDashboard.getToken() +
                        "&IMEI=" + mobileDashboard.getIMEI() +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterSPBJ.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataDivisi.add(responseArray.getJSONObject(i).getString("NamaDept"));
                        dataDivisiID.add(responseArray.getJSONObject(i).getString("DivisiPemohon"));
                    }
                    divisi = responseArray.getJSONObject(0).getString("DivisiPemohon");
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterDivisi.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
        spinnerDivisi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                divisi = dataDivisiID.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ArrayAdapter<String> adapterTipe = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataTipe);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType = (Spinner) findViewById(R.id.spinnerType);
        spinnerType.setAdapter(adapterTipe);
        new OkHttpClient().newCall( new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetTipeSPBJ" +
                        "?Token=" + mobileDashboard.getToken() +
                        "&IMEI=" + mobileDashboard.getIMEI() +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()
        ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                FilterSPBJ.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_cant_connect), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                try {
                    JSONArray responseArray = new JSONArray(responseBody);
                    for (int i = 0; i < responseArray.length(); i++) {
                        dataTipe.add(responseArray.getJSONObject(i).getString("Ket"));
                        dataTipeID.add(responseArray.getJSONObject(i).getString("Tipe"));
                    }
                    tipe = responseArray.getJSONObject(0).getString("Tipe");
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterTipe.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    FilterSPBJ.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), new JSONObject(responseBody).getString("Value"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e1) {
                                Toast.makeText(getApplicationContext(), "API Response Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipe = dataTipeID.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button button = (Button) findViewById(R.id.buttonSearch);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent result = new Intent();
                result.putExtra("dari", dariTanggal);
                result.putExtra("sampai", sampaiTanggal);
                result.putExtra("statusSPBJ", statusSPBJ);
                result.putExtra("statusApproval", statusApproval);
                result.putExtra("project", project);
                result.putExtra("divisi", divisi);
                result.putExtra("tipe", tipe);

                setResult(RESULT_OK, result);

                finish();
            }
        });

        Fabric.with(this, new Crashlytics());
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Filter SPBJ/Approval"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.resetFilter:
                dariTanggal = String.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)) + "-"
                        + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + "-"
                        + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
                sampaiTanggal = String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) + "-"
                        + namaBulan[Calendar.getInstance().get(Calendar.MONTH)]
                        + "-" + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
                textDari.setText(String.valueOf(Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH)) + " "
                        + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                        + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
                textSampai.setText(String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) + " "
                        + namaBulan[Calendar.getInstance().get(Calendar.MONTH)] + " "
                        + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
                spinnerDivisi.setSelection(0);
                spinnerProject.setSelection(0);
                spinnerStatusApproval.setSelection(0);
                spinnerStatusSPBJ.setSelection(0);
                spinnerType.setSelection(0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.approval_notification, menu);

        MenuItem item = menu.findItem(R.id.notification);
        MenuItemCompat.setActionView(item, R.layout.notification_badge);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        badgeNotificationApproval = (TextView) layout.findViewById(R.id.actionbar_notifcation_textview);
        ImageView image = (ImageView) layout.findViewById(R.id.image);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterSPBJ.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        badgeNotificationApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterSPBJ.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterSPBJ.this, ApprovalNotification.class);
                intent.putExtra("SPBJID", sp.getString("ApprovalDetailSPBJID", ""));
                startActivity(intent);
            }
        });

        getCountNotifApproval();

        return super.onCreateOptionsMenu(menu);
    }

    public void getCountNotifApproval() {
        new OkHttpClient().newCall(new Request.Builder()
                .url(mobileDashboard.APIURL + "/GetCountNotifApproval" +
                        "?IMEI=" + mobileDashboard.getIMEI() +
                        "&Token=" + mobileDashboard.getToken() +
                        "&SPBJID=" + sp.getString("ApprovalDetailSPBJID", "") +
                        "&UserID=" + sp.getString("MDUserID", ""))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String notificationCountApproval = response.body().string();
                FilterSPBJ.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if ( badgeNotificationApproval != null) {
                            badgeNotificationApproval.setText(notificationCountApproval);
                            if (!notificationCountApproval.equals("0")) {
                                badgeNotificationApproval.setVisibility(View.VISIBLE);
                            } else {
                                badgeNotificationApproval.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        getCountNotifApproval();
    }
}
