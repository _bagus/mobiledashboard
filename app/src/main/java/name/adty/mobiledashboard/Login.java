package name.adty.mobiledashboard;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.google.firebase.iid.FirebaseInstanceId;

import io.fabric.sdk.android.Fabric;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import name.adty.mobiledashboard.approval.ApprovalDetailTab;
import name.adty.mobiledashboard.approval.ApprovalForum;
import name.adty.mobiledashboard.global.MobileDashboard;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity {

    SharedPreferences sp;
    SharedPreferences.Editor spe;
    private static String TAG = "MDLogin";
    MobileDashboard mobileDashboard;
    String firebaseToken = "";
    private EditText editTextUsename;
    private EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        mobileDashboard = new MobileDashboard(getApplicationContext());

        sp = getSharedPreferences("MobileDashboard", Context.MODE_PRIVATE);
        spe = sp.edit();
        String loginStatus = sp.getString("MobileDashboardLogin", "");
        if(loginStatus.equals("1"))
        {
            startActivity(new Intent(getApplicationContext(), Main.class));
            finish();
        }

        firebaseToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "firebaseToken: " + firebaseToken);
        if (firebaseToken != null && !firebaseToken.equals("null") && !firebaseToken.isEmpty()) {
            spe.putString("firebaseToken", firebaseToken);
            spe.commit();
        } else {
            spe.putString("firebaseToken", "");
            spe.commit();
        }

        editTextUsename = (EditText) findViewById(R.id.editUsername);
        editTextPassword = (EditText) findViewById(R.id.editPassword);

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        final Button buttonSignIn = (Button) findViewById(R.id.buttonLogin);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sp.getString("firebaseToken", "").equals("")) {
                    firebaseToken = FirebaseInstanceId.getInstance().getToken();
                    Log.d(TAG, "firebaseToken: " + firebaseToken);
                    if (firebaseToken != null && !firebaseToken.equals("null") && !firebaseToken.isEmpty()) {
                        spe.putString("firebaseToken", firebaseToken);
                        spe.commit();
                    } else {
                        spe.putString("firebaseToken", "");
                        spe.commit();
                    }

                    buttonSignIn.setText("LOG IN");
                } else {
                    spe.putString("username", editTextUsename.getText().toString());
                    spe.commit();

                    if (sp.getString("firebaseToken", "").equals("")) {
                        Toast.makeText(Login.this, "Tidak dapat mengambil token notifikasi dari server. " +
                                "Tekan tombol untuk mencoba lagi", Toast.LENGTH_LONG).show();
                        buttonSignIn.setText("DAPATKAN TOKEN");
                    } else {
                        editTextUsename.clearFocus();
                        editTextUsename.setEnabled(false);
                        editTextPassword.clearFocus();
                        editTextPassword.setEnabled(false);
                        buttonSignIn.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);

                        doLogin();
                    }
                }
            }
        });

        editTextUsename.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (!editTextUsename.getText().toString().isEmpty() && !editTextPassword.getText().toString().isEmpty()) {
                        buttonSignIn.performClick();
                        return true;
                    }
                }

                return false;
            }
        });

        editTextPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (!editTextUsename.getText().toString().isEmpty() && !editTextPassword.getText().toString().isEmpty()) {
                        buttonSignIn.performClick();
                        return true;
                    }
                }

                return false;
            }
        });

        getPermissionToReadIMEI();
        getPermissionToAccessExternalStorage();

        Fabric.with(this, new Crashlytics());

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Login"));
    }

    private void doLogin() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Content-Type", "text/plain")
                .url(mobileDashboard.APIURL + "/Login?Token=" + mobileDashboard.getToken()
                        + "&IMEI=" + mobileDashboard.getIMEI() + "&UserID=" + editTextUsename.getText().toString()
                        + "&Password=" + mobileDashboard.MD5(editTextPassword.getText().toString()) +
                        "&FirebaseID=" + sp.getString("firebaseToken", "")
                )
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseString = response.body().string();
                JSONObject responseJSON = null;
                try {
                    responseJSON = new JSONObject(responseString);
                    if (responseJSON != null && responseJSON.has("Message")
                            && responseJSON.get("Message").toString().equals("response")
                            && responseJSON.has("Value") && responseJSON.get("Value").toString().equals("1")) {

                        Answers.getInstance().logLogin(new LoginEvent()
                                .putMethod("App Post Data")
                                .putSuccess(true));

                        spe.putString("MobileDashboardLogin", "1");
                        spe.putString("MDUserID", responseJSON.get("Username").toString());
                        spe.putString("MDNamaUser", responseJSON.get("Nama").toString());
                        spe.putString("MDUserRole", responseJSON.get("Role").toString());
                        spe.putString("MDUserJabatan", responseJSON.get("Jabatan").toString());
                        spe.putBoolean("MDUserMenuReport", responseJSON.getBoolean("Report"));
                        spe.putBoolean("MDUserMenuApproval", responseJSON.getBoolean("Approval"));
                        spe.putString("firebaseToken", firebaseToken);
                        spe.putString("MDUserPassword", mobileDashboard.MD5(editTextPassword.getText().toString()));
                        spe.commit();
                        startActivity(new Intent(getApplicationContext(), Main.class));
                        finish();
                    } else {
                        Answers.getInstance().logLogin(new LoginEvent()
                                .putMethod("App Post Data")
                                .putSuccess(false));

                        final JSONObject finalResponseJSON = responseJSON;
                        Login.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(getApplicationContext(), finalResponseJSON.get("Value").toString(), Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Button buttonLogIn = (Button) findViewById(R.id.buttonLogin);
                                buttonLogIn.setVisibility(View.VISIBLE);
                                ProgressBar progressBar1 = (ProgressBar) findViewById(R.id.progressBar);
                                progressBar1.setVisibility(View.GONE);
                                EditText editTextUsename = (EditText) findViewById(R.id.editUsername);
                                editTextUsename.setEnabled(true);
                                EditText editTextPassword = (EditText) findViewById(R.id.editPassword);
                                editTextPassword.setEnabled(true);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {

        } else if (requestCode == 2) {

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void getPermissionToReadIMEI() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        }
    }

    public void getPermissionToAccessExternalStorage() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }
    }
}
